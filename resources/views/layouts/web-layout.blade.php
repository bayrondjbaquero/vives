<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')@yield('line','- ')Vivies Belleza</title>
    <meta meta name="description" content="">
    <link rel="shortcut icon" href="{{ asset('images/iconito.ico') }}" type="image/x-icon">
    <meta name="robots" content="Index, Follow">
    <meta name="keywords" content="">
    <meta name="author" content="DoDigital">
    <meta name="language" content="es">
    <meta name="generator" content="Laravel PHP, HTML5, CCS3, MySql">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lato.css') }}" rel="stylesheet">
    <link href="{{ asset('css/personal.css') }}" rel="stylesheet">
    <!-- toast-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.min.css')}}">
    @stack('style')
</head>
<body class="pt--5">
    <div class="body"></div>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-negro fixed-top p-4">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav mx-auto">


                    @if(Request::url() == "http://localhost:8000/inicio")
                        <a class="nav-lin mr-5 e" href="{{ url('/') }}">
                            <img src="{{ asset('images/Logo-vives.png') }}" alt="" class="img-fluid animated fadeInDown">
                        </a>
                        <li class="nav-item ">
                            <a class="nav-link text-white text-bold e" href="http://localhost:8000/#quienes-somos">Quienes somos</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link text-white text-bold e" href="http://localhost:8000/#nuestros-servicios">Servicios</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link text-white text-bold e" href="{{ url('http://localhost:8000/#contacto') }}">Contacto</a>
                        </li>
                        <li class="nav-item ml-3">
                            <div class="social-media hidden-xs animated bounceIn">
                                <a href="https://www.facebook.com/vivesbellezactg/" target="_blank">
                                    <i class="fa fa-facebook text-white rounded-circle"></i>
                                </a>
                                <a href="https://www.instagram.com/vivesbelleza/" target="_blank">
                                    <i class="fa fa-instagram text-white rounded-circle"></i>
                                </a>
                            </div>
                        </li>

                        @else()
                        <a class="nav-lin mr-5 e" href="{{ url('#carouselExampleControls') }}">
                            <img src="{{ asset('images/Logo-vives.png') }}" alt="" class="img-fluid animated fadeInDown">
                        </a>
                        <li class="nav-item ">
                            <a class="nav-link text-white text-bold e" href="{{ url('#quienes-somos') }}">Quienes somos</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link text-white text-bold e" href="{{ url('#nuestros-servicios') }}">Servicios</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link text-white text-bold e" href="{{ url('#contacto') }}">Contacto</a>
                        </li>
                        <li class="nav-item ml-3">
                            <div class="social-media hidden-xs animated bounceIn">
                                <a href="https://www.facebook.com/vivesbellezactg/" target="_blank">
                                    <i class="fa fa-facebook text-white rounded-circle"></i>
                                </a>
                                <a href="https://www.instagram.com/vivesbelleza/" target="_blank">
                                    <i class="fa fa-instagram text-white rounded-circle"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                </ul>
            </div>
        </nav>
        @yield('content')
        <footer class="bg-negro" id="contacto">
            @yield('contact')
            <div class="text-white text-regular text-center bg-gris p-2 small" style="
                    position:  fixed;
                    bottom:  0;
                    width: 100%;">
                © Copyright 2017. Todos los derechos reservados
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src='{{ asset('js/app.js') }}'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsEfNtuczpMycIvZMtCqgyTVpbtzRjqkU&libraries=geometry"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
    <script>
    window.onload = function() {
      var recaptcha = document.forms["myForm"]["g-recaptcha-response"];
      recaptcha.required = true;
      recaptcha.oninvalid = function(e) {
        // do something
        alert("Debes aceptar no soy un robot");
      }
    }
  </script>


    <script>
        var myCenter=new google.maps.LatLng(10.387872, -75.495160);
        function initialize()
        {
        var mapProp = {
        center:myCenter,
        zoom:16,
        zoomControl: true,
        scaleControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        var map=new google.maps.Map(document.getElementById("googleMaps"),mapProp);
        var marker=new google.maps.Marker({
        position:myCenter,
        // icon: './images/marker.png',
        // content: "hola",
        animation:google.maps.BOUNCE
        });
        marker.setMap(map);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


    @stack('script')
</body>
</html>
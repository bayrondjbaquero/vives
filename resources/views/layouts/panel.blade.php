<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('titulo_dash', 'Administrador') - Vives Belleza</title>
    <meta name="author" content="DoDigital">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description" content="">
    <!-- Font Awesome -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Custom Theme Style -->
    <link href="{{asset('css/custom.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/datatables.min.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        .btn-language,.btn-language-2{
            display: inline-block;
            padding: 10px 15px;
        }
        .btn-language-2{
            background-color: #eb5b2a;
            color: #fff;
        }
        .mycontent-left {
            border-right: 1px dashed #333;
        }
    </style>
    @yield('styles')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;" align="center">
                    <a href="" class="site_title" style="line-height:55px;"><span style="display: inline-block;margin-left: 8px;vertical-align: middle;">
                            {{--logo--}}
                            <img src="{{asset('')}}" alt="Vives Belleza" width="120" height="40" class="img-responsive"></span> </a>
                </div>

                <div class="clearfix"></div>

                <!-- menu perfil -->
                {{--<div class="profile">--}}
                    {{--<div class="profile_pic">--}}
                        {{--<img src="{{asset('img/profile.svg')}}" alt="perfil-admin" class="img-circle profile_img">--}}
                        {{--<br>--}}
                    {{--</div>--}}
                    {{--<div class="profile_info">--}}
                        {{--<span>Bienvenido/a,</span>--}}
                        {{--<h2>{{Auth::guard('web_usuario')->user()->name}}</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- /menu perfil -->

                <br />
                <!-- Indicators -->

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            @if(Auth::guard('web_usuario')->user()->type_user == "Administrador")
                            <li><a><i class="fa fa-newspaper-o" aria-hidden="true"></i>Citas<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('panel-administrativo/listado-citas') }}">Listado de Citas</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-newspaper-o" aria-hidden="true"></i>Empleados<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('panel-administrativo/listado-empleados') }}">Listado Empleado</a></li>
                                    <li><a href="{{ url('panel-administrativo/listado-empleados/create') }}">Agregar Empleado</a></li>

                                </ul>
                            </li>


                            <li><a><i class="fa fa-tags" aria-hidden="true"></i>Clientes<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('panel-administrativo/listado-clientes') }}">Listado Clientes</a></li>
                                </ul>
                            </li>


                            <li><a><i class="fa fa-calendar" aria-hidden="true"></i> Administradores <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('panel-administrativo/administrador/create') }}">Agregar Administrador</a></li>
                                    <li><a href="{{ url('panel-administrativo/administrador') }}">Listado de Administradores</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-building" aria-hidden="true"></i>Servicios<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a href="{{ url('panel-administrativo/tipo-de-servicios') }}">Listado Tipos de Servicios</a></li>
                                    <li><a href="{{ url('panel-administrativo/servicios') }}">Listado de Servicios</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-building" aria-hidden="true"></i> Configuracion<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('panel-administrativo/configuracion') }}">Precio Transporte</a></li>
                                    <li><a href="{{ url('panel-administrativo/barrios') }}">Barrios</a></li>

                                </ul>
                            </li>
                                @elseif(Auth::guard('web_usuario')->user()->type_user == "Cliente")
                                <li>
                                    <a href="{{url('panel-cliente')}}"><i class="fa fa-book" aria-hidden="true"></i> Citas </a>
                                </li>
                                <li>
                                    <a href="{{url('panel-cliente/perfil')}}"><i class="fa fa-user" aria-hidden="true"></i> Perfil</a>
                                </li>
                                <li>
                                    <a href="{{url('panel-cliente/tus-direcciones')}}"><i class="fa fa-cab" aria-hidden="true"></i>Tus Direcciones</a>
                                </li>
                            @endif


                                <li><a href="{{ url('/logout') }}">Cerrar Sesion</a></li>
                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="right" title="Cerrar sesión" class="pull-right" href="" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <span class="fa fa-sign-out" aria-hidden="true"></span>
                    </a>
                    {{--
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>--}}
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <!--<img src="http://placehold.it/100x100" alt="">--> <i class="fa fa-user"></i> Administrador
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> Mi cuenta</a></li>
                                <li><a href="{{route('logout')}}"> Salir</a></li>
                                {{--
                                <li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>
                                --}}
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left" style="width:100%">
                    <h3 class="text-rojo">@yield('titulo_page')</h3>
                </div>

            </div>

            <div class="clearfix"></div>
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Administrador Vives - 2018
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- Custom Theme Scripts -->
{{--<script src="{{asset('js/app.js')}}"></script>--}}
<script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
{{--<script src="{{asset('js/jquery.min.js')}}"></script>--}}
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<script src="{{asset('js/custom.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/datatables.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script>
    $(document).ready(function () {
        $('.table').DataTable({
            "language": {
                "url": "{{asset('js/Spanish.json')}}"
            }
        });
    });
</script>

@stack('scripts')




</body>
</html>

@extends('layouts.web-layout')

@section('title')Ingresa @endsection

@push('style')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css') }}">
    <style>
        .body{
            display: block;
        }
    </style>
@endpush

@section('content')
    <div id="login-vives" class="mt-4">
        <div class="container">
            <div class="row justify-content-center mb-3 mt-3">
                <div class="col-lg-8 col-md-10">

                    @if($errors->has('email'))
                        @foreach($errors->get('email') as $item)
                                <div class="container">
                                    <div class="alert alert-danger">
                                        {{$item}}
                                    </div>
                                </div>
                        @endforeach
                    @endif

                    @if($errors->has('cc'))
                            @foreach($errors->get('cc') as $item)
                                <div class="container">
                                    <div class="alert alert-danger">
                                        {{$item}}
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    <h4 class="text-uppercase text-title text-bold mb-0">1. ingresa con tu cédula</h4>
                    <div class="form-log-container box">
                        <form id="form-log" class="form-horizontal" action="{{url('log')}}" method="POST">
                            {{csrf_field()}}
                            <div class="logo-container box">
                                <img src="{{asset('dist/images/logo-blanco.svg')}}" alt="" class="img-responsive">
                            </div>
                            <div class="form-group">
                                <input class="form-control bg-gray" type="email" name="email" id="email" required="" placeholder="C.C.">
                            </div>
                            <div class="form-group">
                                <input class="form-control bg-gray" type="password" name="password" required="" placeholder="PASSWORD">
                            </div>
                            <div class="form-group text-left">
                                <div class="col-xs-12">
                                    <div align="right" class="d-flex justify-content-between w-100 pl-2 ml-1"> {{--ajustar tipo de letra y tamaño alinear con el boton siguiente--}}
                                        <a href="{{url('recuperar-contrasena')}}" style="color: #0b0b0b">Recuperar contraseña</a>

                                        <button class="text-uppercase btn rounded-0 btn-md bg-amarillo text-white text-bold h5" type="submit">SIGUIENTE</button>
                            </div>
                        </form>
                        <br>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <h4 class="text-uppercase text-title text-bold mb-1">¿PRIMERA VEZ?</h4>
                        <span class="d-block text-uppercase _text-light">INGRESA TUS DATOS</span>
                        <form id="form-log" class="form-horizontal" action="{{url('agregarCliente')}}" method="POST">
                            {{csrf_field()}}
                            <div class="logo-container box">
                                <img src="{{asset('dist/images/logo-blanco.svg')}}" alt="" class="img-responsive">
                            </div>
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input class="form-control bg-gray" type="text" name="nombre" required placeholder="Nombre y Apellido">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control bg-gray" type="number" name="cc" required="" placeholder="C.C">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control bg-gray" type="password" name="password" required="" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control bg-gray" type="password" name="password" onblur="confirmar()" required="" placeholder="Confirmar Password">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control bg-gray" type="text" name="telefono" required="" placeholder="Teléfono">
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <select class="form-control bg-gray" name="barrio" id="barrio">
                                                <option disabled selected>Barrio</option>
                                                @foreach($objBarrios as $barrio)
                                                    <option value="{{$barrio->id}}">{{$barrio->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-5 p-md-0">
                                            <input class="form-control bg-gray" type="text" name="direccion" required placeholder="Dirección">
                                        </div>
                                        <div class="col-md-4">
                                            <input class="form-control bg-gray" type="text" name="punto_referencia" required placeholder="Punto de referencia">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control bg-gray" type="email" name="email" placeholder="Correo">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control bg-gray date-vives" type="text" name="fecha" placeholder="Fecha de cumpleaños">
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input ml-0" id="acepto" required>
                                        <label class="form-check-label text-bold" for="acepto">Acepto condiciones y restricciones</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-justify _text-light">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor.</p>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div align="right">
                                    <button class="btn rounded-0 btn-md bg-amarillo text-white _text-bold" type="submit">SIGUIENTE</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js') }}"></script>
    <script>
        $('.date-vives').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
    </script>
@endpush



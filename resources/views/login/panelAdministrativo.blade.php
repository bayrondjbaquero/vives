@extends('layouts.web-layout')

@push('style')
    <link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.print.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-fullcalendar.css') }}">

    {{-- <script src='lib/jquery.min.js'></script> --}}
    <style>
        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
    </style>
@endpush

@section('content')

    <div class="container " xmlns="http://www.w3.org/1999/html">

                <div class="form-log-container box">
                    <form id="form-log" class="form-horizontal" action="{{url('log')}}" method="POST">
                        {{csrf_field()}}
                        <div class="logo-container box">
                            <img src="{{asset('dist/images/logo-blanco.svg')}}" alt="" class="img-responsive">
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <label for="email">Cuenta</label>
                                <input class="form-control" type="email" name="email" required="" placeholder="Correo">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="password">Contraseña</label>
                                <input class="form-control" type="password" name="password" required="" placeholder="Contraseña">
                            </div>
                        </div>
                        <div class="form-group text-left">
                            <div class="col-xs-12" align="right">
                                <button class="btn rounded-0 btn-md bg-amarillo text-white text-bold" type="submit">
                                    Aceptar
                                </button>
                            </div>
                        </div>

                    </form>
                    <div class="clearfix"></div>

                </div>

            </div>
        </div>
    </div>

@endsection




@extends('layouts.web-layout')

@section('content')
<div class="content  d">
    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img src="{{ asset('images/banner-principal.png') }}" alt="" class="img-fluid mx-auto w-100">
            </div>
            <div class="carousel-item  ">
                <img src="{{ asset('images/baner2.jpg') }}" alt="" class="img-fluid mx-auto w-100">
            </div>
            <div class="carousel-item  ">
                <img src="{{ asset('images/baner3.jpg') }}" alt="" class="img-fluid mx-auto w-100">
            </div>
            <div class="carousel-item  ">
                <img src="{{ asset('images/baner4.jpg') }}" alt="" class="img-fluid mx-auto w-100">
            </div>
            <div class="carousel-item  ">
                <img src="{{ asset('images/baner5.jpg') }}" alt="" class="img-fluid mx-auto w-100">
            </div>
            <div class="carousel-item  ">
                <img src="{{ asset('images/baner6.jpg') }}" alt="" class="img-fluid mx-auto w-100">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="fa fa-chevron-left rounded-circle" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="fa fa-chevron-right rounded-circle" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="row agenda">
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-md-6 p-3 text-center">
                <div class="text-bold text-negro h4">
                    <i class="fa fa-calendar mr-3"></i> 
                    AGENDA TU CITA
                    <a href="{{ url('/inicio') }}" class="ml-3 btn text-bold bg-amarillo btn-lg text-white border-0 rounded-0">HAZ CLIC AQUÍ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container " id="quienes-somos">
    <div class="row justify-content-center p-5">
        <div class="col-md-5 nosotros">
            <img src="{{ asset('images/quienes_somos.png') }}" alt="" class="img-fluid d-block mx-auto ">
        </div>
        <div class="col-md-5">

            <h4 class="text-bold text-negro text-center">QUIENES SOMOS</h4>

            <p class="text-justify text-dark text-regular">
                Somos una empresa dedicada a brindar servicios de belleza 
                a domicilio, dando solución a una necesidad de la
                población cartagenera, facilitando el acceso fexible de las
                personas al cuidado de la salud y la belleza desde la tranqui-
                lidad y comodidad de su hogar u ofcina, con un equipo de
                profesionales altamente califcados, amables y dispuestos a
                ofrecer una grata experiencia de servicio con calidad, conf-
                anza, seguridad y confort.
            </p>
        </div>
    </div>
</div>
<div class="row justify-content-center p-5 servicios bg-dark" id="nuestros-servicios">
    <div class="container">
        <h4 class="text-bold text-negro text-center text-white">SERVICIOS</h4><br>
        <div class="row p-3">
            @foreach($servicios as $servicio)
            <div class="col-md-3 animated fadeInUp" data-toggle="modal" data-target="#exampleModal{{$servicio->id}}" style="cursor: pointer;">
                <img src="{{ asset('images/servicios-'.$loop->iteration.'.png') }}" alt="" class="img-fluid d-block mx-auto">
                <div class="text-white text-center pt-3 mx-auto" style="max-width: 100px;">
                    <span class="text-bold text-uppercase">{{$servicio->name}}</span>
                </div>
            </div>
            <div class="modal fade" id="exampleModal{{$servicio->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel{{$servicio->id}}" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content" style="background-color: #000;color: #fff;">
                  <div class="modal-header" style="background-color: rgba(0, 0, 0, 0.81);border-bottom:1px solid rgba(233, 236, 239, 0.12);">
                    <h5 class="modal-title ama" id="exampleModalLabel{{$servicio->id}}">{{$servicio->name}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="{{ asset('images/servicios-'.$loop->iteration.'.png') }}" alt="" class="img-fluid d-block mx-auto">
                    <ul class="mt-2 list-unstyled pl-3 pr-3">
                    @foreach($servicio->servicios as $item)
                    <li class="d-flex justify-content-between"> <p class="w-75">{{$item->name}}</p> <p>$ {{$item->price}}</p></li>
                    @endforeach
                    </ul>
                  </div>
                  <div class="modal-footer" style="border-top: 1px solid rgba(233, 236, 239, 0.12);">
                    <button type="button" class="btn rounded-0 bg-amarillo text-white text-bold" data-dismiss="modal">Cerrar</button>
                    {{-- <button type="button" class="btn btn-primary">Salvar</button> --}}
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            {{-- <div class="col-md-3 animated fadeInUp">
                <img src="{{ asset('images/servicios-1.png') }}" alt="" class="img-fluid d-block mx-auto">
                <div class="text-white text-center pt-3">
                    <span class="text-bold text-uppercase">cuidado</span><br>
                    <span class="text-regular text-uppercase">capilar</span>
                </div>
            </div>
            <div class="col-md-3 animated fadeInUp">
                <img src="{{ asset('images/servicios-2.png') }}" alt="" class="img-fluid d-block mx-auto">
                <div class="text-white text-center pt-3">
                    <span class="text-bold text-uppercase">cuidado</span><br>
                    <span class="text-regular text-uppercase">de uñas</span>
                </div>
            </div>
            <div class="col-md-3 animated fadeInUp">
                <img src="{{ asset('images/servicios-3.png') }}" alt="" class="img-fluid d-block mx-auto">
                <div class="text-white text-center pt-3">
                    <span class="text-bold text-uppercase">cuidado</span><br>
                    <span class="text-regular text-uppercase">corporal</span>
                </div>
            </div>
            <div class="col-md-3 animated fadeInUp">
                <img src="{{ asset('images/servicios-4.png') }}" alt="" class="img-fluid d-block mx-auto">
                <div class="text-white text-center pt-3">
                    <span class="text-bold text-uppercase">cuidado</span><br>
                    <span class="text-regular text-uppercase">facial</span>
                </div>
            </div> --}}
        </div>
    </div>
</div>
@endsection

@section('contact')
    <div class="container">
        <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
            <li class="nav-item bor">
                <a class="nav-link active border-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                    <p class="h4 text-white text-center text-bold text-uppercase d-block mx-auto pt-1">HABLA CON NOSOTROS</p>
                </a>
            </li>
            <li class="nav-item bor">
                <a class="nav-link border-0" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                    <p class="h4 text-white text-center text-bold text-uppercase d-block mx-auto pt-1">PQRSD</p>
                </a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                {{-- <p class="h4 text-white text-center text-bold text-uppercase d-block mx-auto pt-3">HABLA CON NOSOTROS</p> --}}
                <div class="row pt-4 pl-4 pr-4 justify-content-center mb-5">
                    {{-- @if(count($errors)!= 0)
                        <div class="alert alert-warning text-white" role="alert">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif --}}
                    <div class="col-md-7">
                        {!! Form::open(array('url'=> '/contacto','method'=> 'POST', 'files' => true , 'id' => 'myForm', )) !!}
                        {!! Form::token() !!}
                            <div class="form-group">
                                <!-- <label for="exampleFormControlInput1">Email address</label> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control rounded-0 text-muted bg-gris" name="nombre" id="exampleFormControlInput2" placeholder="Nombres (requerido):" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control rounded-0 text-muted bg-gris" name="email" id="exampleFormControlInput1" placeholder="Tu correo electrónico (requerido):" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control rounded-0 text-muted bg-gris" name="mensaje" id="exampleFormControlTextarea1" rows="4" placeholder="Mensaje:"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="g-recaptcha" data-sitekey="6LchWD4UAAAAAGx8aPLyIeALI5ZO2zav23mrxDKU" ></div>
                                    </div>
                                    <div class="col-md-2 p-0 text-left"><br>
                                        <input type="submit" class="btn rounded-0 btn-sm bg-amarillo text-white text-bold" value="ENVIAR">
                                    </div>
                                    <div class="col-md-5">
                                        <address class="text-left text-white text-regular small">
                                            <i class="fa fa-phone ama"></i>
                                            Teléfono: (5)(+57)311 720 4395 <br>
                                            <i class="fa fa-send ama"></i>
                                            info@vivesbelleza.com<br>
                                            Cartagena de Indias, Colombia<br>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}

                        @if(session()->has('success'))

                            <div class="container">

                                <div class="alert alert-success">{{session('success')}}
                                    <button type="button" class="close" data-dismiss="alert">x</button>
                                </div>

                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                {{-- <p class="h4 text-white text-center text-bold text-uppercase d-block mx-auto pt-3">PQR</p> --}}
                <div class="row pt-4 pl-4 pr-4 justify-content-center mb-5">
                    {{-- @if(count($errors)!= 0)
                        <div class="alert alert-warning text-white" role="alert">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif --}}
                    <div class="col-md-7">
                        {!! Form::open(array('url'=> '/pqr','method'=> 'POST', 'files' => true , 'id' => 'myForms', )) !!}
                        {!! Form::token() !!}
                            <div class="form-group">
                                <!-- <label for="exampleFormControlInput1">Email address</label> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control rounded-0 text-muted bg-gris" name="nombre" id="exampleFormControlInput" placeholder="Nombres (requerido):" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control rounded-0 text-muted bg-gris" name="email" id="exampleFormControlInput" placeholder="Tu correo electrónico (requerido):" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <select name="asunto" class="form-control rounded-0 text-muted bg-gris" id="">
                                    <option value="0">Seleccionar</option>
                                    <option value="queja">Quejas</option>
                                    <option value="reclamos">Reclamos</option>
                                    <option value="sugerencia">Sugerencia</option>
                                    <option value="felicitaciones" selected="selected">Felicitaciones</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control rounded-0 text-muted bg-gris" name="mensaje" id="exampleFormControlTextarea1" rows="4" placeholder="Mensaje:"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="submit" class="btn rounded-0 btn-md bg-amarillo text-white text-bold" value="ENVIAR">
                                    </div>
                                    <div class="col-md-5">
                                        <address class="text-left text-white text-regular small">
                                            <i class="fa fa-phone ama"></i>
                                            Teléfono: (5)(+57)311 720 4395 <br>
                                            <i class="fa fa-send ama"></i>
                                            info@vivesbelleza.com<br>
                                            Cartagena de Indias, Colombia<br>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        jQuery(document).ready(function($) {
            $(".navbar a.e").on('click', function(e) {
                $('.navbar a.e').removeClass('df');
                $('.navbar a.e').parent('li').removeClass('active');
                e.preventDefault();
                var enlace = $(this).attr('href');
                $('html, body').animate({
                    scrollTop: $(enlace).offset().top - 120
                }, 300);
                $(this).addClass('df');
                $(this).parent('li').addClass('active');
            });
        });
    </script>
@endpush
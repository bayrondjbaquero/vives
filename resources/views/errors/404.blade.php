@extends('layouts.web-layout')

@section('content')
	<div class="container">
		<div class="row p-5">
			<p class="h1 text-center text-muted d-block mx-auto text-regular">UPS! La página que buscas no se encuentra.</p>
		</div>
	</div>
@endsection
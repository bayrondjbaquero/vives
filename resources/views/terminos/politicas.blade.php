@extends('layouts.web-layout')

@section('content')
    <div class="content">

        <div align="center">
            <h2>POLÍTICA DE PRIVACIDAD</h2>
        </div>
        <div class="container text-justify">
            <p class="text-justify">

            VIVES, Belleza a un Clic, recopila y utiliza los datos personales de los usuarios del sistema de agendamiento en línea en la medida
            y para los fines descritos a continuación. <br><br>

            <h5>I. Acceso al sistema de agendamiento de citas en línea.</h5>

            Al acceder al sistema de agendamiento de citas en línea, su dispositivo, por razones técnicas,
            transmite datos al servidor web de VIVES. Los siguientes datos, para la comunicación entre su dispositivo y
            el servicio web, se recopilan únicamente de forma temporal y se utilizan con el propósito de transmitir la información que ha solicitado y
            son requeridas por razones técnicas para tal fin: <br><br>

            <li>
                Nombre completo.
            </li>
            <li>
                Cedula.
            </li>
            <li>
                Dirección en donde quiere recibir el servicio con punto de referencia para mejor ubicación.
            </li>
            <li>
                Teléfono de contacto.
            </li>
            <li>
                Email a donde quiere recibir la confirmación de su cita agendada, confirmación de cancelación de cita y para recibir información de promociones y descuentos en fechas especiales.
            </li>
            <br>
            VIVES, no hará uso de sus datos personales para fines diferentes a los ya mencionados, todo en el marco legal del habeas Data según la Ley 1266 del 2008 y Ley 1581 del 2012.<br><br>


            <h5> II. Uso del formulario de contacto</h5>

            Usted puede contactar VIVES directamente haciendo uso del formulario de contacto disponible en el sistema de agendamiento de citas en línea.
            VIVES utilizará la información recopilada por este medio, únicamente con el fin de procesar su solicitud. <br><br>

            VIVES trabaja de forma conjunta para el manejo del formulario de contacto y procesamiento de la comunicación con usted según lo
            estipulado en las leyes 1266 de 2008 y la 1581 del 2012 y garantiza el seguimiento y cumplimiento de los términos de privacidad,
            así como todas las demás medidas de seguridad necesarias.<br><br>

            <h5>Clientes</h5>

            Si utiliza el sistema de agendamiento de citas en línea como Cliente del Proveedor de Servicios, VIVES recopilará y utilizará la información personal en
            la medida y para los fines descritos a continuación:<br><br>

            <h6>  I. Tipo y alcance del uso y recolección de datos</h6>
            Al registrarse en el sistema de agendamiento de citas en línea como Cliente, VIVES recopilará y utilizará sus siguientes datos personales: <br><br>

            <li>Nombre y apellido </li>
            <li>Cedula </li>
            <li>Número del teléfono móvil</li>
            <li>Dirección</li>
            <li>Correo electrónico</li>
            <li>Contraseña</li>
            <li>VIVES recopilará además los siguientes datos en el momento en que un Cliente reserve una cita no vinculante suministrado por el Proveedor de Servicios:</li>
            <li>Servicio seleccionado</li>
            <li>Profesional seleccionado para llevar a cabo el servicio</li>
            <li>La hora de reserva seleccionada</li>
            <li>Datos de facturación</li>
            <br>
            <h6> II. Propósito de uso y recolección de datos</h6>
            VIVES utiliza los datos suministrados por los Clientes para la prestación de sus servicios se salud y belleza descritos detalladamente en las Condiciones de uso Clientes,
            en particular lo referente al uso del sistema de agendamiento de citas en línea.<br><br>

            VIVES utiliza también la información recopilada sobre los Clientes en forma anónima y para usos estadísticos, con el fin de mejorar y optimizar el sistema de reservas de citas en línea.<br><br>

            En caso de que el Cliente haya dado su consentimiento expreso para tal fin, VIVES y el Proveedor de Servicios pueden hacer uso de los datos personales obtenidos de los Clientes para ofrecerle al
            Cliente información sobre productos y ofertas personalizadas de VIVES, la cual será enviada por correo electrónico o mediante el envío de notificaciones en el sistema de agendamiento de citas
            en línea.<br><br>

            Usted puede revocar este consentimiento con efecto futuro, y de manera gratuita, enviando un correo electrónico a info@vivesbelleza.com.<br><br>

            <h5> C. Protección de sus datos</h5>
            VIVES protege el sistema de agendamiento de citas en línea, y los sistemas informáticos, con las herramientas técnicas y
            organizativas necesarias para evitar la pérdida, daño, acceso no autorizado, modificación y distribución de sus datos por parte de personas no autorizadas.<br><br>

            <h5> D. Derecho a la información, corrección, bloqueo y eliminación de sus datos</h5>

            Usted tiene el derecho a obtener información de forma gratuita sobre los datos que se han almacenados en lo referente a su persona y, si corresponde, el derecho a la
            corrección, bloqueo o eliminación de estos datos.<br><br>

            Usted puede en todo momento y con efecto futuro, revocar su consentimiento para la recopilación, procesamiento y uso de estos datos.<br><br>

            VIVES, está disponible para responder cualquiera de sus preguntas en relación con nuestras políticas de protección de datos y el procesamiento de sus datos personales.
            Puede ponerse en contacto con VIVES al correo electrónico info@vivesbelleza.com. o utilizando los datos de contacto establecidos en nuestra plataforma web.<br><br>

            <h5> E. Modificaciones </h5>
            VIVES se reserva el derecho a modificar la presente Política de Privacidad, en el momento que así lo crea conveniente. Toda modificación entrará en efecto al
            ser publicada en el sistema de reserva de citas en línea. Por lo anterior, VIVES recomienda revisar continuamente nuestro sistema de agendamiento de citas en línea a
            fin de conocer las últimas modificaciones llevadas a cabo.<br><br>

            Última modificación: 22 Marzo 2018.<br><br>

            </p>
        </div>
    </div>


@endsection
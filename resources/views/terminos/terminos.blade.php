@extends('layouts.web-layout')

@section('content')
    <div class="content">

        <div class="container text-justify">
            <p class="text-justify">

            <h2 align="center">TERMINOS Y CONDICIONES DE USO</h2>
            <h5>General</h5>
                VIVES, desde su página web ofrece una herramienta para el agendamiento de citas en línea con la finalidad de que sus clientes accedan fácil y
            raídamente a los servicios ofrecidos. Los clientes registrados como usuarios de VIVES, tienen la posibilidad de ver los servicios que se ofrece y
            que pueden ser objeto de reservas en línea.<br><br>


            <h5>1. Objeto del Servicio</h5>
                El contenido y el alcance de los servicios proporcionados por VIVES, se rigen por estas Condiciones de Uso, para lo demás, se definen de
                acuerdo a las funciones disponibles en el sistema de agendamiento de citas en línea de forma fija o temporal.<br><br>

                VIVES tiene el derecho a modificar las funcionalidades que ofrece esta plataforma y de poner a disposición nuevas y/o suspender
                el módulo de agendamiento en línea en el momento que lo crea conveniente. En todo caso, y en todo momento, al hacerlo, VIVES tendrá en
                cuenta los intereses legítimos de sus Clientes usuarios de los servicios.<br><br>

            <h5>2. Cuenta del Cliente y Datos de Acceso </h5>

                Nuestra plataforma web, con el fin de reservar las citas agendadas por sus clientes y darles cumplimiento dentro de las condiciones definidas
                para ello, debe crear una cuenta de Cliente. Para esto, el Cliente debe ingresar su nombre y apellido, cedula, dirección de residencia o en la cual
                quiere recibir el servicio a domicilio, correo electrónico, teléfono móvil y/o fijo de contacto y contraseña. Con este correo electrónico y contraseña,
                el cliente podrá acceder al sistema de agendamiento de citas en línea (en adelante, “Datos de Acceso”). El Cliente garantiza que la información suministrada por
                el/ella es en todo momento veraz y completa. <br><br>
                Es obligación del Cliente VIVES el mantener la confidencialidad de los Datos de Acceso para el ingreso a su cuenta. El cliente debe informar de inmediato
                y por escrito a VIVES, cuando un tercero, y sin autorización, haya accedido a sus Datos de Acceso. El Cliente acepta ser responsable del manejo y uso que
                estos terceros puedan hacer al utilizar sus Datos de Acceso, a menos que se compruebe la no culpabilidad del Cliente en lo referente a tales acciones no autorizadas.<br><br>

            <h5>3. Reserva de Citas</h5>

                a. El Cliente, para agendar una cita mediante el sistema de agendamiento de citas en línea de los servicios ofrecidos VIVES, debe escoger el servicio deseado, la hora y la fecha
                específica en que desee que ésta se lleve a cabo. El equipo de VIVES, una vez recibida su solicitud seleccionará el profesional que llevará a cabo el servicio.<br><br>
                b. Durante el proceso del agendamiento de la cita en línea, el Cliente cuenta con la posibilidad de revisar su información para asegurarse de que no exista ningún error,
                y en caso necesario, corregirla.<br><br>
                c. Se considera que el Cliente completa el proceso de agendamiento de la cita haciendo clic en el botón destinado para tal fin. La reserva de una cita en línea no crea
                una obligación del Cliente o de VIVES para la celebración de un contrato para llevar a cabo el servicio determinado, pero si se toma como un como u compromiso serio
                de prestación de servicios, el cual puede ser cancelado, suspendido o reprogramado con la debida antelación para evitar gastos innecesarios en desplazamientos o movilización
                de los profesionales de VIVES y la  disposición efectiva del servicio hacia un cliente que lo requiera generando así liberación del espacio reservado en la agenda para ser utilizado.<br><br>
                d. Las cancelaciones de citas agendadas para los servicios que ofrece VIVES, deben hacerse con mínimo 2 horas previas a la hora inicialmente asignada de la cita.
                En caso de no cumplir con esta condición, el cliente debe asumir el costo del desplazamiento del profesional VIVES para la ejecución del servicio, si este se alcanza a realizar.<br><br>
                e. Una vez finalizado el proceso de reserva, el Cliente recibe una confirmación automática de la reserva por correo electrónico.<br><br>

            <h5> 4. Licencias</h5>

                a. La autorización del Cliente se limita al acceso al sistema de agendamiento de citas en línea, y a dar uso de las funciones y características disponibles en el sistema
                en ese momento, de acuerdo con lo establecido en estas condiciones de uso.<br><br>
                b. A menos que en estas Condiciones de Uso o en el sistema de reservas de citas lo permita explícitamente, o haya sido habilitada una función del sistema de reserva que
                    así lo permita, el Cliente sólo podrá acceder y visualizar el contenido disponible en el sistema de reserva en línea única y exclusivamente para su uso personal,
                    y por el tiempo de duración en que use el sistema de reserva. El Cliente no está autorizado a modificar, cambiar, traducir, mostrar, publicar, exhibir,
                    copiar o distribuir, en parte o en su totalidad, el contenido disponible en el sistema de agendamiento de citas. El Cliente no está en ningún caso autorizado,
                    y en especial, a eliminar la información de derechos de autor, logos y/o otras marcas o notas de protección del contenido del sistema de agendamiento de citas en línea.<br><br>

            <h5>5. Protección de Datos </h5>

                VIVES le da gran importancia a la protección de los datos personales y únicamente recopila, procesa y utiliza los datos personales de sus Clientes según
                lo estipulado por las leyes aplicables a la protección de datos, y de acuerdo con los principios descritos en la Política de Privacidad.<br><br>

            <h5>6. Cancelación y/o modificación de citas agendadas</h5>
                1. Una vez agendada una cita para recibir cualquiera de los servicios que ofrece VIVES, el cliente recibirá un correo electrónico automático con la
                confirmación de su cita, el cual incluye los detalles de la cita como fecha y hora, además, se relacionan los datos del profesional de VIVES que brindará el
                servicio solicitado. En la parte inferior de ese mismo correo, se habilitará una opción de cancelación y/o modificación de la cita asignada, ingresando a ese
                enlace podrá realzar la operación que requiera en ese momento.<br><br>
                2. Una vez se cancele o modifique una cita a través del link respectivo contenido en el correo de confirmación, se generará un correo automático
                al equipo VIVES que permitirá realizar los ajustes necesarios con sus profesionales asignados y de esa manera se libera ese espacio en la agenda, el cual podrá
                ser ocupada por otro cliente que lo requiera.<br><br>
                3. Este proceso de cancelación y/o modificación de citas asignadas debe realizarse con mínimo 2 horas previas a la hora asignada inicialmente, en caso de no
                cumplirse con esta condición el cliente debe asumir los gastos que representen el desplazamiento del profesional, en caso de que así haya ocurrido y será el
                mismo valor liquidado durante el proceso de agendamiento de su cita.
            </p>
    </div>
    </div>


@endsection
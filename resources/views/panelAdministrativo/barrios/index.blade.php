@extends('layouts.panel')
@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}</div>

        </div>

    @endif

    <a href="/panel-administrativo/barrios/create" class="btn btn-primary btn-lg">
        Agregar Barrio
    </a>

    <div class="table-responsive">
        <table id="listado-equipos" class="table table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Slug</th>
                <th>Precio</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($barrios as $barrio)
                <tr data-id="{{$barrio->id}}">
                    <td> {{$barrio->name}}</td>
                    <td> {{$barrio->slug}}</td>
                    <td> {{$barrio->price}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/barrios/'.$barrio->id.'/edit')}}"> Editar </a>
                        {{--<button class="btn btn-danger btn-bEliminar" >Eliminar</button>--}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection

@push('scripts')
    <script>

        $('.btn-bEliminar').on('click', function (e) {
            e.preventDefault();
            if (confirm('Realmente desea eliminar este barrio del sistema')){
                var fila = $(this).parents('tr');
                var id = fila.data('id');
                $.ajax({
                    type: 'DELETE',
                    url: '/panel-administrativo/barrios/' + id,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {

                        location.reload();

                    },
                    error:function (data) {
                        alert('Barrio no eliminado ' + data);
                    }
                });
            }else{
                alert('Barrio no eliminado');
            }

        });

    </script>
@endpush
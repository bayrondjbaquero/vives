@extends('layouts.panel')

@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

    <div align="center">
        <h2>EDITAR BARRIO</h2>
    </div>

    <div class="container">

                {!! Form::model($objBarrio,['url'=>['panel-administrativo/barrios',$objBarrio->id], 'method'=>'PUT', 'files' => true, 'id' =>'editarAdministrador', 'class'=>'form-horizontal form-label-left']) !!}

                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="name" name="name" required="required"  value="{{$objBarrio->name}}" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Precio <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="price" name="price" required="required"  value="{{$objBarrio->price}}" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div align="right">

            <button type="submit" class="btn btn-primary">Guardar</button>
            <a href="/panel-administrativo/barrios" class="btn btn-danger">Cancelar</a>
        </div>


                {!! Form::close() !!}

    </div>



@endsection
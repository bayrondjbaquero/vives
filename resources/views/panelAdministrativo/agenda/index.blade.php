@extends('layouts.panel')

@section('content')


    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

    <div class="table-responsive">
        <table id="listado-galeria" class="table table-bordered">
            <thead>
            <tr>
                <th>Servicios</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Cliente</th>
                <th>Trabajador</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($objListado as $agenda)
                <tr data-id="{{$agenda->id}}">
                    <td>{{$agenda->servicios}}<br>
                        @if($agenda->largo_cabello != null)
                           <strong>Largo del Cabello: </strong> {{$agenda->largo_cabello}} <br>
                        @endif
                        @if($agenda->color_actual !=null)
                            <strong>Color Actual: </strong>{{$agenda->color_actual}}<br>
                        @endif
                        @if($agenda->color_aplicar != null)
                            <strong>Color a Aplicar: </strong>{{$agenda->color_aplicar}}
                        @endif
                        <br>

                    </td>
                    <td>{{$agenda->date}}</td>
                    <td>{{$agenda->hour_start}}</td>
                    <td> <strong>Nombre: </strong>{{$agenda->clientes->usuario->name}} <br>
                        <strong>Identificacion:</strong> {{$agenda->clientes->identification}} <br>
                        <strong>Telefono:</strong> {{$agenda->clientes->usuario->phone}} <br>
                        <strong>Direccion: </strong>{{$agenda->clientes->barrio->name}}, {{$agenda->clientes->address_reference}}
                    </td>
                    @if($agenda->employees_id)
                    <td>{{$agenda->trabajador->usuario->name}}</td>
                    @else
                        <td class="bg-danger">No tiene  asignado un trabajador</td>
                    @endif

                    <td>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/asignar/'.$agenda->id)}}"> Asignar Trabajador </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection
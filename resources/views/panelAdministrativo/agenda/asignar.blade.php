@extends('layouts.panel')

@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif
{{--Asignar varios trabajadores a la agenda--}}
    <form action="{{url('panel-administrativo/asignarTrabajador')}}" method="POST" id="addBarrio" class="form-horizontal form-label-left">

        {{ csrf_field() }}

        <input type="hidden" id="agenda" name="agenda" value="{{$id}}">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Trabajador<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" id="trabajador" name="trabajador">
                    <option disabled selected>Seleccione un trabajador</option>
                    @foreach($trabajadores as $tipo)
                        <option value="{{$tipo->id}}">{{$tipo->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <a href="/panel-administrativo/listado-citas" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <button type="submit" class="btn btn-primary">Aceptar</button>
        </div>

    </form>
@endsection

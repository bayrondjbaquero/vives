@extends('layouts.web-layout')

@section('title')Seleccionar Servicios @endsection

@push('style')
    <style>.body{display: block;}</style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <style>
        body{
            padding-top: 12px !important;
        }
        .exito{
        	position:absolute;
        	left:0;
        	right:0;
        	top:0;
        	bottom:0;
        	height: 100vh;
        }
    </style>
@endpush

@section('content')
	<div class="exito w-100 d-flex justify-content-center">
		<div class="d-flex justify-content-center align-items-center flex-column">
			<h4 class="text-white">SU CITA HA SIDO AGENDADA CON ÉXITO</h4>
			<a class="btn rounded-0 btn-md bg-amarillo text-white text-bold text-uppercase" href="/panel-cliente/">
				VOLVER AL INICIO
			</a>
		</div>
	</div>
    <img src="images/fondo.jpg">
@endsection


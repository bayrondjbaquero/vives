@extends('layouts.web-layout')

@section('title')Cambio de contraseña @endsection

@push('style')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css') }}">
    <style>
        .body{
            display: block;
        }
    </style>
@endpush

@section('content')
    <div id="login-vives" class="mt-4">
        <div class="container">
            <div class="row justify-content-center mb-3 mt-3">
                <div class="col-lg-8 col-md-10">

                    @if(session('flash') == "correo")

                        <div class="container">

                            <div class="alert alert-success">
                                Se ha cambiado la contraseña con exito
                                <button type="button" class="close" data-dismiss="alert">x</button>
                            </div>

                        </div>
                    @elseif(session('flash') == "DI")
                        <div class="container">
                            <div class="alert alert-danger">
                                Error! no se pudo cambiar la contraseña
                                <button type="button" class="close" data-dismiss="alert">x</button>
                            </div>
                        </div>
                    @endif


                    <h4 class="text-uppercase text-title text-bold mb-0">Recuperar Contraseña</h4>
                    <div class="form-log-container box">
                        <form id="form-log" class="form-horizontal" action="{{url('cambioPassword')}}" method="POST" autocomplete="off">
                            {{csrf_field()}}
                            <div class="logo-container box">
                                <img src="{{asset('dist/images/logo-blanco.svg')}}" alt="" class="img-responsive">
                            </div>
                            <div class="form-group">
                                <label class="">Correo</label>
                                <input class="form-control bg-gray" type="email" name="email" id="email" required="" placeholder="Correo" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label class="">Codigo</label>
                                <input class="form-control bg-gray" type="text" name="solicitud" id="solicitud" required="" placeholder="Codigo de solicitud" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="">Nueva Contraseña</label>
                                <input class="form-control bg-gray" type="password" name="password" id="password" required="" placeholder="Contraseña" autocomplete="off">
                            </div>

                            <div class="form-group text-left">
                                <div class="col-xs-12">
                                    <div align="right">

                                        <button class="text-uppercase btn rounded-0 btn-md bg-amarillo text-white text-bold h5" type="submit">SIGUIENTE</button>
                                    </div>
                        </form>
                        <br>
                        <div class="clearfix"></div>
                    </div>
                    <div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js') }}"></script>
    <script>
        $('.date-vives').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
    </script>
@endpush



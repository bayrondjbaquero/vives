@extends('layouts.panel')
@section('content')

    <div class="table-responsive">
        <table id="listado-equipos" class="table table-bordered">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Servicios</th>
                <th>Precio</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($citas as $cita)
                <tr data-id="{{$cita->id}}">
                    <td> {{$cita->date}}</td>
                    <td> {{$cita->hour_start}}</td>
                    <td> {{$cita->servicios}}</td>
                    <td> {{$cita->price_total}}</td>
                    <td>
                        <a class="btn btn-danger" href="{{url('panel-administrativo/listado-empleados/'.$cita->id.'/edit')}}"> Cancelar </a>

                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
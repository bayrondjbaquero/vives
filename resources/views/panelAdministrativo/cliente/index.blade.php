@extends('layouts.web-layout')

@push('style')
<link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.print.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap-fullcalendar.css') }}">

{{-- <script src='lib/jquery.min.js'></script> --}}
<style>
    #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
</style>
@endpush

@section('content')
    <div id="calendar"></div>
    <div class="container " xmlns="http://www.w3.org/1999/html">
        <div class="row justify-content-center mb-3 mt-3">
            <div class="col-md-10">
                
                @if(session()->has('flash'))

                    <div class="container">

                        <div class="alert alert-danger">
                            {{session('flash')}}
                            <button type="button" class="close" data-dismiss="alert">x</button>
                        </div>

                    </div>
                @endif
                <h2>1.INGRESA CON TU CÈDULA</h2>
                <div class="form-log-container box">
                    <form id="form-log" class="form-horizontal" action="{{url('log')}}" method="POST">
                        {{csrf_field()}}
                        <div class="logo-container box">
                            <img src="{{asset('dist/images/logo-blanco.svg')}}" alt="" class="img-responsive">
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <label for="email">Cuenta</label>
                                <input class="form-control" type="number" name="cc" required="" placeholder="Cèdula">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="password">Contraseña</label>
                                <input class="form-control" type="password" name="password" required="" placeholder="Contraseña">
                            </div>
                        </div>
                        <div class="form-group text-left">
                            <div class="col-xs-12" align="right">
                                <button class="btn rounded-0 btn-md bg-amarillo text-white text-bold" type="submit">
                                    Aceptar
                                </button>
                            </div>
                        </div>

                    </form>
                    <div class="clearfix"></div>



                </div>

            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src='{{ asset('vendor/fullcalendar/lib/moment.min.js') }}'></script>
    <script src='{{ asset('vendor/fullcalendar/lib/jquery.min.js') }}'></script>
    <script src='{{ asset('vendor/fullcalendar/fullcalendar.min.js') }}'></script>
    <script>
        $(document).ready(function() {
            $('#calendar').fullCalendar({
              header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
              },
              defaultDate: '2018-02-12',
              navLinks: true, // can click day/week names to navigate views
              selectable: true,
              selectHelper: true,
              select: function(start, end) {
                var title = prompt('Event Title:');
                var eventData;
                if (title) {
                  eventData = {
                    title: title,
                    start: start,
                    end: end
                  };
                  $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                }
                $('#calendar').fullCalendar('unselect');
              },
              editable: true,
              eventLimit: true, // allow "more" link when too many events
              events: [
                {
                  title: 'All Day Event',
                  start: '2018-02-01'
                },
                {
                  title: 'Long Event',
                  start: '2018-02-07',
                  end: '2018-02-10'
                },
                {
                  id: 999,
                  title: 'Repeating Event',
                  start: '2018-02-09T16:00:00'
                },
                {
                  id: 999,
                  title: 'Repeating Event',
                  start: '2018-02-16T16:00:00'
                },
                {
                  title: 'Conference',
                  start: '2018-02-11',
                  end: '2018-02-13'
                },
                {
                  title: 'Meeting',
                  start: '2018-02-12T10:30:00',
                  end: '2018-02-12T12:30:00'
                },
                {
                  title: 'Lunch',
                  start: '2018-02-12T12:00:00'
                },
                {
                  title: 'Meeting',
                  start: '2018-02-12T14:30:00'
                },
                {
                  title: 'Happy Hour',
                  start: '2018-02-12T17:30:00'
                },
                {
                  title: 'Dinner',
                  start: '2018-02-12T20:00:00'
                },
                {
                  title: 'Birthday Party',
                  start: '2018-02-13T07:00:00'
                },
                {
                  title: 'Click for Google',
                  url: 'http://google.com/',
                  start: '2018-02-28'
                }
              ]
            });

          });
    </script>
    {{-- <script src='{{ asset('vendor/fullcalendar/lib/moment.min.js') }}'></script>
    <script src="{{ asset('vendor/fullcalendar/lib/jquery.min.js') }}"></script>
    <script src='{{ asset('vendor/fullcalendar/fullcalendar.min.js') }}'></script> --}}
    {{-- <script src='{{ asset('vendor/fullcalendar/locale/es-us.js') }}'></script> --}}
@endpush



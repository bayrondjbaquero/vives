@extends('layouts.web-layout')

@section('title')Seleccionar Servicios @endsection

@push('style')
    <style>.body{display: block;}</style>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-datetimepicker.css') }}">

@endpush

@section('content')
    <div class="container mt-5">
        <form id="addAgenda" class="form-horizontal" action="{{url('guardar')}}" method="POST" autocomplete="false">
        <div class="row justify-content-center mb-3 mt-3">



            <div class="col-lg-6 offset-lg-1">
                @if(session()->has('flash'))
                    <div class="container">
                        <div class="alert alert-danger">{{session('flash')}}</div>
                    </div>
                @endif
                <h4 class="text-uppercase text-title text-bold mb-4">2. ESCOGE EL SERVICIO</h4>
                <div class="form-log-container box">

                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="date" class="form-control" name="fecha" id="fecha" required> <br>

                                <input id="hora_inicio" name="hora_inicio" type="text" class="form-control input-small">


                                <br>
                                @foreach($objTipoServicio as $tipo)
                                    <div class="form-group">
                                    <strong class="d-block mb-2">{{$tipo->name}}</strong>
                                    @foreach($objServicios as $servicio)
                                       @if($tipo->id == $servicio->type_services_id)
                                            <input type="checkbox" id="servicios['{{$servicio->id}}']" name="servicios[]"
                                                   value="{{$servicio->name}}" onclick="agregarHoras({{$servicio->num_hour}}, {{$servicio}} ,{{$servicio->price}} ,$(this))">
                                                <label for="servicios{{$servicio->id}}" class="d-inline-block _text-light mb-2">
                                                    {{$servicio->name}}
                                                </label>
                                                <br>
                                        @endif
                                    @endforeach
                                    </div>
                                @endforeach
                                 <br>
                                {{--<button class="btn rounded-0 btn-md bg-amarillo text-white text-bold text-uppercase" type="submit">FINALIZAR</button>--}}
                            </div>
                        </div>

                    <div class="clearfix"></div>
                  {{--</form>--}}
                </div>
                <br>
            </div>
            <div class="col-lg-5">
                <div class="login-right p-4">
                    <div class="form-group">
                        <span class="d-inline-block text-bold">Hora:</span><p class="_text-light d-inline-block mb-0 pl-2" id="txtHora"> </p>
                    </div>
                    <div class="form-group">
                        <h4 class="text-title text-bold text-uppercase">Servicios</h4>
                    </div>
                    <div class="form-group" id="listServicios">
                     {{--div dinamicos cuando se seleccionan los servicios--}}
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <span class="text-bold text-uppercase">TRANSPORTE</span><p class="">$ {{$barrio}} COP</p>
                            <input type="hidden" value="{{(int)$barrio}}" id="intValor" name="intValor">
                        </div>
                        <div class="d-flex justify-content-between">
                            <span class="text-bold text-uppercase">TOTAL</span><p class="" id="txtTotal"></p>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input ml-0" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1"><h4 class="text-title text-bold text-uppercase">Responda estas preguntas antes de finalizar</h4></label>
                </div>
                <div class="form-group pl-3">
                    <input class="bg-gray form-control" id="largo_cabello" name="largo_cabello" type="text" placeholder="Largo del cabello">
                </div>
                <div class="form-group pl-3">
                    <input class="bg-gray form-control" id="color_actual" name="color_actual" type="text" placeholder="Color actual">
                </div>
                <div class="form-group pl-3">
                    <input class="bg-gray form-control" id="color_desea" name="color_desea" type="text" placeholder="Color que desea aplicar">
                </div>
                <div class="form-group text-left">
                    <div class="col-xs-12" align="right">
                        <button class="btn rounded-0 btn-md bg-amarillo text-white text-bold text-uppercase" type="submit">FINALIZAR</button>
                    </div>
                </div>

            </div>
        </div>
        </form>
    </div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/mo/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('js/es.js') }}"></script>

    <script>

        $('#hora_inicio').bootstrapMaterialDatePicker({
            lang : 'es',
            date: false,

        });

        //$('#hora_inicio').timepicker({showMeridian: false, defaultTime: false});
        numHoras=0;
        precio=0;
        transporte = parseInt(document.getElementById("intValor").value);
        total = 0;
        function agregarHoras(horas,nombre, valor,checkbox){

            if(checkbox.is(":checked")){

                console.log("price "+nombre["price"]);

                numHoras =+ numHoras + horas;
                // numHoras+horas;

                precio += parseInt(nombre["price"]);

                var capa = document.getElementById("listServicios");
                var div = document.createElement("div");
                div.innerHTML = "<div id=\"divSe"+nombre["id"]+"\" " +"class=\"d-flex justify-content-between\">\n" +
                                "<span class=\"_text-light\">"+nombre["name"] +"</span><p class=\"\">$"+ nombre["price"].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +" COP</p>\n" +
                                "</div>";

                capa.appendChild(div);

                total = parseInt(precio) + parseInt(transporte);
                resl =total.toString();
                document.getElementById("txtHora").innerHTML = "Su Servicio inicia a "+ document.getElementById("hora_inicio").value +" tiene un tiempo de "+numHoras+" minutos";
                document.getElementById("txtTotal").innerHTML = "$ "+resl+".000 COP";

            }else{
                numHoras =+ numHoras - horas;
                // numHoras - horas;
                precio =+ precio - parseInt(valor);

                idDiv = "divSe"+nombre["id"];
                $("#"+idDiv).remove();

                total = parseInt(precio) + parseInt(transporte);
                resl =total.toString();
                document.getElementById("txtHora").innerHTML = "Su Servicio inicia a "+ document.getElementById("hora_inicio").value +" tiene un tiempo de "+numHoras+" minutos";
                document.getElementById("txtTotal").innerHTML = "$ "+resl.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +".000 COP";

            }
        }

        $('#addAgenda').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: '/guardar/'+numHoras+'/'+total,
                data: $('#addAgenda').serialize(),
                success: function(data, textStatus, jqXHR){

                    toastr["success"](data, "Notificacion Vives");
                    console.log(data);
                    location.href ="http://localhost:8000/fin-agenda";

                },
                error:function(data, textStatus, errorThrown){
                    toastr["error"](data.responseText, "Error Vives");
                }
            });
        });
    </script>
@endpush



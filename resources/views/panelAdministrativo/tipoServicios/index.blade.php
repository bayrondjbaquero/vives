@extends('layouts.panel')
@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}</div>

        </div>

    @endif

    <a href="/panel-administrativo/tipo-de-servicios/create" class="btn btn-primary btn-lg">
        Agregar Tipo de Servicio
    </a>

    <div class="table-responsive">
        <table id="listado-equipos" class="table table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Slug</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($objTipoServicios as $tipo)
                <tr data-id="{{$tipo->id}}">
                    <td> {{$tipo->name}}</td>
                    <td> {{$tipo->slug}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/tipo-de-servicios/'.$tipo->id.'/edit')}}"> Editar </a>
                        {{--<a class="btn btn-danger" href="{{url('panel-administrativo/barrios/'.$tipo->id.'/edit')}}"> ELiminar </a>--}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
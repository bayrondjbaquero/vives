@extends('layouts.panel')

@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

    <form action="{{url('panel-administrativo/tipo-de-servicios')}}" method="POST" id="addBarrio" class="form-horizontal form-label-left">

        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>



        <div class="modal-footer">
            <a href="/panel-administrativo/tipo-de-servicios" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <button type="submit" class="btn btn-primary">Aceptar</button>
        </div>

    </form>
@endsection

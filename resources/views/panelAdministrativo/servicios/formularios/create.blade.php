@extends('layouts.panel')

@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

    <form action="{{url('panel-administrativo/servicios')}}" method="POST" id="addBarrio" class="form-horizontal form-label-left">

        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="nombre" name="nombre" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tiempo <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="number" id="tiempo" name="tiempo" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Precio <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="number" id="precio" name="precio" step="0.001" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Servicio <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <select class="form-control" id="tipo_servicio" name="tipo_servicio">
                   <option disabled selected>Seleccione un tipo de servicio</option>
                   @foreach($objTipoServicios as $tipo)
                       <option value="{{$tipo->id}}">{{$tipo->name}}</option>
                       @endforeach
               </select>
            </div>
        </div>
        <div class="modal-footer">
            <a href="/panel-administrativo/servicios" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <button type="submit" class="btn btn-primary">Aceptar</button>
        </div>

    </form>
@endsection

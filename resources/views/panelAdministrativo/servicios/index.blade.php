@extends('layouts.panel')
@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}</div>

        </div>

    @endif

    <a href="/panel-administrativo/servicios/create" class="btn btn-primary btn-lg">
        Agregar Servicio
    </a>

    <div class="table-responsive">
        <table id="listado-servicios" class="table table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Tiempo</th>
                <th>Precio</th>
                <th>Slug</th>
                <th>Tipo Servicio</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($objServicios as $servicio)
                <tr data-id="{{$servicio->id}}">
                    <td> {{$servicio->name}}</td>
                    <td> {{$servicio->num_hour}} minutos</td>
                    <td> $ {{$servicio->price}}</td>
                    <td> {{$servicio->slug}}</td>
                    <td> {{$servicio->tipoServicios->name}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/servicios/'.$servicio->id.'/edit')}}"> Editar </a>
                        {{--<a class="btn btn-danger" href="{{url('panel-administrativo/barrios/'.$tipo->id.'/edit')}}"> ELiminar </a>--}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
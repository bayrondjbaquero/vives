@extends('layouts.panel')
@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}</div>

        </div>

    @endif

    <a href="/panel-administrativo/listado-empleados/create" class="btn btn-primary btn-lg">
        Agregar Empleado
    </a>

    <div class="table-responsive">
        <table id="listado-equipos" class="table table-bordered">
            <thead>
            <tr>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Identificacion</th>
                <th>Telefono</th>
                <th>Correo</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($objEmpleados as $empleado)
                <tr data-id="{{$empleado->id}}">
                    <td> <img src="../images/{{$empleado->usuario->photo}}" style="height: 70px"></td>
                    <td> {{$empleado->usuario->name}}</td>
                    <td> {{$empleado->usuario->identification}}</td>
                    <td> {{$empleado->usuario->phone}}</td>
                    <td> {{$empleado->usuario->email}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/listado-empleados/'.$empleado->id)}}"> Editar </a>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/tarjeta/'.$empleado->id)}}"> Tarjeta </a>
                        {{--<a class="btn btn-danger" href="{{url('panel-administrativo/listado-empleados/'.$empleado->id.'/edit')}}"> ELiminar </a>--}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
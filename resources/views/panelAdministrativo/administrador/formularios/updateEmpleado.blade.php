@extends('layouts.panel')

@section('content')

    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

  {!! Form::model($objEmpleado,['url'=>['panel-administrativo/editarEmpleado',$objEmpleado->id], 'method'=>'PUT', 'files' => true,'id' =>'editarEmpleado']) !!}


    {{--<form action="{{url('panel-administrativo/editarEmpleado')}}" method="PUT" id="addEmpleado" enctype="multipart/form-data">--}}
    {{ csrf_field() }}
    <div align="center">
        <img src="/images/{{$objEmpleado->usuario->photo}}" style="height: 70px">
    </div>
        <input type="hidden" id="id" name="id" value="{{$objEmpleado->id}}">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre y Apellido <span class="required">*</span>
            </label>
            <div class="">
                <input type="text" id="name" name="name" value="{{$objEmpleado->usuario->name}}" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto <span class="required">*</span>
        </label>
        <div class="">
            <input type="file" id="photo" name="photo" value="{{$objEmpleado->usuario->photo}}"  class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Identificacion<span class="required">*</span>
        </label>
        <div class="">
            <input type="number" id="identification" name="identification" value="{{$objEmpleado->usuario->identification}}" required="required" class="form-control col-md-7 col-xs-12">
        </div>
    </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telefono <span class="required">*</span>
            </label>
            <div class="">
                <input type="number" id="phone" name="phone" value="{{$objEmpleado->usuario->phone}}" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
            </label>
            <div class="">
                <input type="email" id="email" name="email"  value="{{$objEmpleado->usuario->email}}" required="required" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña <span class="required">*</span>
            </label>
            <div class="">
                <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="modal-footer">

        </div>

    <div class="modal-footer">
        <a href="/panel-administrativo/listado-empleados" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <button type="submit" class="btn btn-primary">Aceptar</button>
    </div>



    {!! Form::close() !!}
@endsection

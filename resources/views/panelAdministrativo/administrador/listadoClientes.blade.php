@extends('layouts.panel')
@section('content')

    <div class="table-responsive">
        <table id="listado-equipos" class="table table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Telefono</th>
                <th>Correo</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($objClientes as $cliente)
                <tr data-id="{{$cliente->id}}">
                    <td> {{$cliente->usuario->name}}</td>
                    <td> {{$cliente->usuario->phone}}</td>
                    <td> {{$cliente->usuario->email}}</td>
                    <td>
                        {{--<a class="btn btn-primary" href="{{url('panel-administrativo/listado-empleados/'.$empleado->id.'/edit')}}"> Editar </a>--}}
                        {{--<a class="btn btn-danger" href="{{url('panel-administrativo/listado-empleados/'.$empleado->id.'/edit')}}"> ELiminar </a>--}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
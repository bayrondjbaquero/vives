@extends('layouts.panel')

@section('content')


    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

    <div class="table-responsive">
        <table id="listado-galeria" class="table table-bordered">
            <thead>
            <tr>
                <th>Nobre</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Opciones</th>

            </tr>
            </thead>
            @foreach($objAdministradores as $administrador)
                <tr data-id="{{$administrador->id}}">
                    <td>{{$administrador->usuario->name}}</td>
                    <td>{{$administrador->usuario->email}}</td>
                    <td>{{$administrador->usuario->phone}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{url('panel-administrativo/administrador/'.$administrador->id.'/edit')}}"> Editar </a>
                        <button class="btn btn-danger btn-dGaleria" >Eliminar</button>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection
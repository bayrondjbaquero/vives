<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
<style>
    @font-face {
        font-family: 'Gotham-Book';
        src: url('Gotham-Book.otf') format('opentype');
        font-style: normal;
    }
    @font-face {
        font-family: 'Gotham-BookIta';
        src: url('Gotham-BookIta.otf') format('opentype');
        font-style: normal;
    }
    @font-face {
        font-family: 'Gotham-Medium';
        src: url('Gotham-Medium.otf') format('opentype');
        font-style: normal;
    }
    .titulo-regular{
        font-family: 'Gotham-Medium';
    }
    .titulo{
        font-family: 'Gotham-Medium';
        font-size: 35px;
    }
    .subtitulo{
        font-family: 'Gotham-Book';
        font-size: 18px;
    }
    .blanco{
        color: white;
    }
    .azul{
        color:#FFFFFF;
    }
    .amarillo{
        color: #fde294;
    }
    .body{
        font-family: 'Gotham-Medium';
        color: #797979;
        font-size: 18px;
    }
    .body2{
        font-family: 'Gotham-Medium';
        color: #fff;
        font-size: 18px;
    }
    a{
        color: inherit !important;
        text-decoration: none !important;
    }
    table{
        margin: 0 auto;
        -webkit-box-shadow: 1px 1px 1px 1px lightgray;
        -moz-box-shadow: 1px 1px 1px 1px lightgray;
        -o-box-shadow: 1px 1px 1px 1px lightgray;
        box-shadow: 1px 1px 1px 1px lightgray;
    }
    td.r{
        padding: 0;
        margin: 0;
        display: block;
    }
    .contenido{
        padding: 0;
        margin: 0;
        display: table-cell;
        padding-left: 15px;
    }
</style>
<a class="btn btn-primary" href="/panel-administrativo/listado-empleados">Regresar</a>
<table width="650" border="0" cellspacing="0" cellpadding="0" style="background-color: #0b0b0b">
    <tbody >
    <tr>
        <td class="r">
            {{--<img src="{{ asset('images/'.$usuario->photo) }}" style="height: 300px"alt="">--}}
        </td>
        <td colspan="1" class="contenido">
            <div align="center">
                <img src="{{ asset('images/logo.png')}}">
            </div>
            <div align="center">
                <br><br>
                <span style="" class="titulo azul">nombre</span><br>
            </div>

            <div align="center">
                <span class="subtitulo"><span class="azul titulo-regular"><strong><a href="www.vivesbelleza.com">www.vivesbelleza.com</a></strong></span></span>
            </div>

        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
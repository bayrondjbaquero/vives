@extends('layouts.panel')

@section('content')


    @if(session()->has('flash'))

        <div class="container">

            <div class="alert alert-success">{{session('flash')}}
                <button type="button" class="close" data-dismiss="alert">x</button>
            </div>

        </div>
    @endif

    <div class="table-responsive">
        <table id="listado-galeria" class="table table-bordered">
            <thead>
            <tr>
                <th>Servicios</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Cliente</th>

            </tr>
            </thead>
            @foreach($objCitas as $agenda)
                <tr data-id="{{$agenda->id}}">
                    <td>{{$agenda->servicios}}</td>
                    <td>{{$agenda->date}}</td>
                    <td>{{$agenda->hour_start}}</td>
                    <td>{{$agenda->clientes->usuario->name}}</td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection
<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Auth;
use Closure;

class AuthUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
   }

    public function handle($request, Closure $next)
    {

              if (Auth::guard('web_usuario')->guest()) {
                   if ($request->ajax()) {
                            return response('Unauthorized.', 401);
            } else {
                           return redirect()->guest('/');
            }
       }

        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use App\models\Agenda;
use App\models\Cliente;
use App\models\Empleado;
use App\models\Horario;
use App\models\Servicio;
use App\models\Usuario;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Mail;
use App\Mail\BillSended;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth_user', ['only' => [
            'store',

        ]]);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $horas, $precio)
    {

        if ($request->ajax()){

            $objAgenda = new Agenda();

            $numEmpleados = count(Empleado::all());
            $countServicios = count($request->servicios)-1;

            $servicios="";
            $diaAgenda = Agenda::where('date',$request['fecha'])->get();
            $horaAgenda = Agenda::where('hour_start', $request['hora_inicio'])->get();


            $fechaActual = Carbon::now();
            $fechaActual = $fechaActual->format('Y-m-d');

            $horaActual = Carbon::now();
            $horaActual = ($horaActual->format('H')).":00";



            for ($i = 0; $i <= $countServicios; $i++){

                $servicios = $request->servicios[$i].", ".$servicios;

            }

            if($countServicios == -1){
                return response($var = "Por Favor seleccione un servicio para su cita", 404);

            }else{

                if ( $request['fecha'] < $fechaActual || $horaActual > $request['hora_inicio']){

                    return response($var = "La fecha u hora ingresada es inferior a la actual", 404);

                }elseif (strtotime($request['hora_inicio']) > strtotime('18:00') || strtotime($request['hora_inicio']) < strtotime('8:00')){

                    return response($var = "Lo sentimos para esta hora no estamos disponibles", 404);

                }else{
                    if ($diaAgenda != null){

                        if (count($diaAgenda) >= $numEmpleados && count($horaAgenda) >= $numEmpleados){

                            //                seleccione otro dia u hora
                            return response($var = "Seleccione otra hora o dia para la agenda");

                        }else{

                            $objAgenda->hour_start = $request['hora_inicio'];
    //                        $objAgenda->hour_end = "1:00";
                            $objAgenda->price_total = $precio;
                            $objAgenda->status = "1";
                            $objAgenda->date = $request['fecha'];
                            $objAgenda->servicios = $servicios;
                            $objAgenda->color_actual = $request['color_actual'];
                            $objAgenda->color_aplicar = $request['color_desea'];
                            $objAgenda->largo_cabello = $request['largo_cabello'];
                            //cambiar el cliente por el logueado
                            $usuario = Usuario::findorfail(Auth::guard('web_usuario')->user()->id);

                            $cli=Cliente::where('users_id',$usuario->id)->get();

                            $cliente="";
                            foreach ($cli as $user){
                                $cliente = $user->id;
                            }



                            $objAgenda->clients_id = $cliente;

                            $objAgenda->save();

    //                        $horario = new Horario();
    //
    //                        $horario->agendas_id = $objAgenda->id;
    //                        $horario->
    //                        $horario->save();

                            $nombre = Auth::guard('web_usuario')->user()->name;
                            $para = Auth::guard('web_usuario')->user()->email;
                            $titulo = 'Tu Reserva se ha creado con exito';
                            // colocar direccion en el correo
                            $mensaje = '<html>'.
                                '<head><title>Reserva Creada</title></head>'.
                                '<body><h1>Vives Belleza</h1>'.
                                'Hola '.$nombre.' tu cita en vives belleza se ha creado con exito'.
                                '<hr>'.
                                '<strong>Datos de la reserva</strong>'.
                                'Tu cita se agendo para el dia '.$request['fecha'].'<br>'.
                                'Para la hora '.$request['hora_inicio'].'<br>'.
                                'Con un precio de: $'.$precio.'<br>'.
                                'Tu cita fue agendada para la direccion'.
                                '</body>'.
                                '</html>';

                            $cabeceras = 'MIME-Version: 1.0' . "\r\n";
                            $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                            $cabeceras .= 'From: Vives Belleza <info@vivesbelleza.com>';
                            // enviar mensaje al cliente
                            mail($para, $titulo, $mensaje, $cabeceras);


                            //enviar mensaje a vives
                            $para = 'reservas@vivesbelleza.com';
                            $titulo = 'Nueva Reserva';
                            $mensaje = "<html>
                                <head><title>Nueva Rerserva</title></head>
                                <body>
                                Se ha creado una nueva reserva el dia ".$request['fecha']." para la hora". $request['hora_inicio'] . " , ingresa al panel administrativo para ve mas informacion para la hora<br>
                                </body>
                                </html>";
                            $cabeceras = 'MIME-Version: 1.0' . "\r\n";
                            $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                            $cabeceras .= 'From: Vives Belleza';

                            mail($para, $titulo, $mensaje, $cabeceras);



                            return response()->json($var = "guardar 1", 200);
                        }
                    }else{
                        //guardar/

                        $objAgenda->hour_start = "1:00";
                        $objAgenda->hour_end = "1:00";
                        $objAgenda->price_total = "250";
                        $objAgenda->status = "1";
                        $objAgenda->date = $request['fecha'];

                        $objAgenda->save();

                        return response($var = "Agenda guardada 2", 200);
                    }
                }

            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

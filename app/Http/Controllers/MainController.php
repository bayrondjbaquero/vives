<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\models\TipoServicio;

class MainController extends Controller
{
    public function index(){
    	$servicios = TipoServicio::all();
    	return view('welcome', compact('servicios'));
    }

    public function terminos(){
        return view('terminos.terminos');
    }

    public function politicas(){
        return view('terminos.politicas');
    }

}

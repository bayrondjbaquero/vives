<?php

namespace App\Http\Controllers;

use App\models\Administrador;
use App\models\Barrio;
use App\models\Cliente;
use App\models\Usuario;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Mail;
use URL;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login.inicio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(Auth::guard('web_usuario')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {


            if (Auth::guard('web_usuario')->user()->type_user == "Administrador"){

                return redirect()->intended('panel-administrativo/listado-citas');

            }elseif (Auth::guard('web_usuario')->user()->type_user == "Cliente"){


                return redirect()->intended('panel-cliente/seleccionar-servicio');

            }elseif (Auth::guard('web_usuario')->user()->type_user == "Empleado"){

               return redirect()->intended('panel-trabajador/citas');
            }

        }

        return back()->with('flash','Credenciales incorrectas!');
    }

    public function logout()
    {

        if (Auth::guard('web_usuario')->check()) {
            Auth::guard('web_usuario')->logout();
        }

        return redirect()->to('/');
    }

    public function cargarVistaLogin()
    {

        $objBarrios = Barrio::all();
        return view('login.inicio', compact('objBarrios'));
    }

    public function cargarPanel(){

        return view('login.panelAdministrativo');
    }

    public function solicitudCambio(Request $request){

        $objUsuario = Usuario::where('email',$request['email'])->get();

        if (count($objUsuario) != 0){

            $solicitud = str_random(10);

            $id ="";
            $nombre ="";

            foreach ($objUsuario as $us){
                $id = $us->id;
                $nombre = $us->name;
            }

            $objUsuario = Usuario::findorfail($id);
            $objUsuario->reset = $solicitud;

            $objUsuario->save();

            //envio de correo con el codigo de la solicitud
            $para = $request['email'];
            $titulo = 'Solicitud para cambiar contraseña';

            $mensaje = '<html>'.
                '<head></head>'.
                '<body><h1>Vives Belleza</h1>'.
                'Hola '.$nombre.', se ha creado una solicitud para cambiar la contraseña de tu cuenta en vives belleza'.'<br>'.
                'Ingresa a '.'<a href="http://localhost:8000/cambio-contrasena"> http://localhost:8000/cambio-contrasena </a>'.'<br>'.
                'Con el siguiente codigo '.$solicitud.' para poder seguir con el proceso.'.
                '</body>'.
                '</html>';

            $cabeceras = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $cabeceras .= 'From: Vives Belleza <info@vivesbelleza.com>';
            // enviar mensaje al cliente
            mail($para, $titulo, $mensaje, $cabeceras);

            return back()->with('flash','correo');

        }elseif (count($objUsuario) == 0){
            return back()->with('flash','DI');
        }

    }

    public function cambiarPassword(Request $request){
        $objUsuario = Usuario::where([['email',$request['email']],['reset',$request['solicitud']],['type_user','Cliente']])->get();

        if (count($objUsuario) != 0){

            $id ="";
            foreach ($objUsuario as $us){
                $id = $us->id;
            }

            $usuario = Usuario::findorfail($id);

            $usuario->reset = "";
            $usuario->password = $request['password'];
            $usuario->save();

            return back()->with('flash','correo');

        }elseif (count($objUsuario) == 0){
            return back()->with('flash','DI');
        }
    }

}

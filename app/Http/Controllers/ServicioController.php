<?php

namespace App\Http\Controllers;

use App\models\Servicio;
use App\models\TipoServicio;
use Illuminate\Http\Request;

class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth_user', ['only' => [
            'index',
            'create',
            'store',
            'edit',
            'update',

        ]]);
    }

    public function index()
    {
        $objServicios = Servicio::all();

        return view('panelAdministrativo.servicios.index', compact('objServicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objTipoServicios = TipoServicio::all();

        return view('panelAdministrativo.servicios.formularios.create',compact('objTipoServicios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objServicio = new Servicio();

        $objServicio->name = $request['nombre'];
        $objServicio->price = $request['precio'];
        $objServicio->num_hour = $request['tiempo'];
        $objServicio->type_services_id = $request['tipo_servicio'];
        $objServicio->save();

        return back()->with('flash','Servicio Agregado Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $objServicio = Servicio::findOrFail($id);
      $objTipoServicios = TipoServicio::all();

      return view('panelAdministrativo.servicios.formularios.update',compact('objServicio','objTipoServicios'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objServicio = Servicio::findOrFail($id);

        $objServicio->name = $request['nombre'];
        $objServicio->price = $request['precio'];
        $objServicio->num_hour = $request['tiempo'];
        $objServicio->type_services_id = $request['tipo_servicio'];
        $objServicio->save();

        return back()->with('flash','Servicio Editado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

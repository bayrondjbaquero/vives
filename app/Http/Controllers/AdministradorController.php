<?php

namespace App\Http\Controllers;

use App\models\Administrador;
use App\models\Agenda;
use App\models\Cliente;
use App\models\Empleado;
use App\models\Usuario;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class AdministradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth_user', ['only' => [
            'index',
            'create',
            'edit',
            'destroy',
            'update',
            'store',
           'listadoClientes',
           'listadoEmpleados',
           'formEmpleado',
           'guardarEmpleado',
           'verEmpleado',
           'editarEmpleado',
           'listadoCitas',
           'asignar',
           'asignarTrabajador',
           'tarjeta'
       ]]);
    }

    public function index()
    {
        $objAdministradores = Administrador::all();


        return view('panelAdministrativo.administrador.index',compact('objAdministradores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('panelAdministrativo.administrador.formularios.createAdministrador');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //usuario
        $objUsuario = new Usuario();
        $objUsuario->name = $request['name'];
        $objUsuario->email = $request['email'];
        $objUsuario->password = $request['password'];
        $objUsuario->phone = $request['phone'];
        $objUsuario->verified = 1;
        $objUsuario->type_user = "Administrador";
        $objUsuario->save();

        //relacion con el administrador
        $objAdministrador = new Administrador();

        $objAdministrador->users_id = $objUsuario->id;
        $objAdministrador->save();

        return back()->with('flash','Administrador creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objUsuario = Usuario::findOrfail($id);

        return view('panelAdministrativo.administrador.formularios.updateAdministrador',compact('objUsuario'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objUsuario = Usuario::findOrfail($id);

        $objUsuario->name = $request['name'];
        $objUsuario->email = $request['email'];

        if ($request['password'] != null)
        {
            $objUsuario->password = $request['password'];
        }

        $objUsuario->phone = $request['phone'];
        $objUsuario->save();

        return back()->with('flash','Administrador editado correctamente!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listadoClientes(){

        $objClientes = Cliente::all();

        return view('panelAdministrativo.administrador.listadoClientes',compact('objClientes'));
    }

    public function listadoEmpleados(){

        $objEmpleados = Empleado::all();
        return view('panelAdministrativo.administrador.listadoEmpleados',compact('objEmpleados'));
    }

    public function formEmpleado(){

        return view('panelAdministrativo.administrador.formularios.createEmpleado');
    }

    public function guardarEmpleado(Request $request){

        //usuario
        $objUsuario = new Usuario();
        $objUsuario->name = $request['name'];
        $objUsuario->email = $request['email'];
        $objUsuario->password = $request['password'];
        $objUsuario->phone = $request['phone'];
        $objUsuario->verified = 0;
        $objUsuario->type_user = "empleado";
        $objUsuario->identification = $request['identification'];


        if ($request['photo'] != null){

            $file = Input::file('photo');
            $random = str_random(5);
            $nombre = $random.'-'.$file->getClientOriginalName();

            $path = public_path('images/'.$nombre);

            $imagen = Image::make($request->file('photo')->getRealPath());

            $imagen->save($path);

            $objUsuario->photo = $nombre;
        }

        $objUsuario->save();

        //Empleado
        $objEmpleado = new Empleado();
        $objEmpleado->users_id = $objUsuario->id;
        $objEmpleado->save();

        return back()->with('flash','Empleado Agregado correctamente!');

    }

    public function verEmpleado($id)
    {
        $objEmpleado = Empleado::findOrFail($id);

        return view('panelAdministrativo.administrador.formularios.updateEmpleado',compact('objEmpleado'));

    }

    public function editarEmpleado(Request $request,$id){


        $objEmpleado= Empleado::findOrfail($id);

        $objUsuario = Usuario::findOrfail($objEmpleado->users_id);

        $objUsuario->name = $request['name'];
        $objUsuario->email = $request['email'];
        $objUsuario->identification = $request['identification'];

        if ($request['photo'] != null){

            \File::delete(public_path('images/'.$objUsuario->photo));

            $file = Input::file('photo');
            $random = str_random(5);
            $nombre = $random.'-'.$file->getClientOriginalName();

            $path = public_path('images/'.$nombre);

            $imagen = Image::make($request->file('photo')->getRealPath());

            $imagen->save($path);

            $objUsuario->photo = $nombre;
        }

        if ($request['password'] != null){
            $objUsuario->password = $request['password'];
        }

        $objUsuario->phone = $request['phone'];
        $objUsuario->update();

        return back()->with('flash','Empleado Modificado correctamente!');
    }

    public function listadoCitas (){
        $validar_fecha = Carbon::now();
        $objListado = Agenda::where('date',$validar_fecha->format('Y-m-d'))->get();

            return view('panelAdministrativo.agenda.index',compact('objListado'));
    }

    public function asignar($id){
        $trabajadores = Usuario::where('type_user','Empleado')->get();
        return view('panelAdministrativo.agenda.asignar',compact('id','trabajadores'));
    }

    public function asignarTrabajador(Request $request){


        $usuario = Usuario::findOrfail($request['trabajador']);

        $employe = Empleado::where('users_id', $usuario->id)->get();

        $id = "";
        foreach ($employe as $trab){
            $id = $trab->id;
        }


        $agenda = Agenda::findOrfail($request->agenda);

        $agenda->employees_id = $id;
        $agenda->save();

        //buscar el cliente para enviar el correo
        $idCliente = $agenda->clients_id;

        $cliente = Cliente::findorfail($idCliente);

        $us = Usuario::findorfail($cliente->users_id);

        $para = $us->email;
        $titulo = 'Tu Reserva se ha creado con exito';

        $mensaje = '<html>'.
            '<head><title>Reserva Creada</title></head>'.
            '<body><h1>Vives Belleza</h1>'.
            'Hola '.$us->name.' en las proximas horas estara llegando '.$usuario->name.' para cumplir con la agenda que nos solicitaste'.
            '<hr>'.
            '</body>'.
            '</html>';

        $cabeceras = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $cabeceras .= 'From: Vives Belleza <info@vivesbelleza.com>';
        // enviar mensaje al cliente
        mail($para, $titulo, $mensaje, $cabeceras);

        $validar_fecha = Carbon::now();
        $objListado = Agenda::where('date',$validar_fecha->format('Y-m-d'))->get();

        return view('panelAdministrativo.agenda.index',compact('objListado'));
    }

    public function tarjeta($id){
        $empleado = Empleado::findorfail($id);
        $usuario = Usuario::findorfail($empleado->users_id);

        return view('panelAdministrativo.administrador.tarjetaTrabajador',compact('usuario'));
    }
}

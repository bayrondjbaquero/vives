<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fecha = date('d - M - Y');
        $nombre = $request->input('nombre');
        $email = $request->input('email');
        $mensaje = $request->input('mensaje');

        $para      = "contacto@vivesbelleza.com";
        $titulo    = "Contacto web Vives";
        $mensaje    =  "Fecha: $fecha \r\n\r\n". 
                    "Contacto desde Web Vives \r\n\r\n\r\n".
                    "Nombre Completo: $nombre\r\n".
                    "Correo Electrónico: $email \r\n".
                    "Comentarios: $mensaje \r\n";

        $cabeceras = "From: Contacto web Vives";

        if (mail($para, $titulo, $mensaje, $cabeceras)) {
            return back()->with('success', 'Se envió su solicitud correctamente');
        }else{
            return back()->with('error', 'Se generó un error interno, intentalo de nuevo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pqr(Request $request)
    {
        $fecha = date('d - M - Y');
        $nombre = $request->input('nombre');
        $email = $request->input('email');
        $mensaje = $request->input('mensaje');
        $asunto = $request->input('asunto');

        $para      = "info@vivesbelleza.com";
        $titulo    = "PQR web Vives "."(".$asunto.")";
        $mensaje    =  "Fecha: $fecha \r\n\r\n".
            "Contacto desde Web Vives \r\n\r\n\r\n".
            "Nombre Completo: $nombre\r\n".
            "Correo Electrónico: $email \r\n".
            "Comentarios: $mensaje \r\n";

        $cabeceras = "From: Contacto web Vives";

        if (mail($para, $titulo, $mensaje, $cabeceras)) {
            return back()->with('success', 'Se envió su solicitud correctamente');
        }else{
            return back()->with('error', 'Se generó un error interno, intentalo de nuevo');
        }
    }
}

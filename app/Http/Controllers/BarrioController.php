<?php

namespace App\Http\Controllers;

use App\models\Barrio;
use Illuminate\Http\Request;

class BarrioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth_user', ['only' => [
            'index',
            'create',
            'store',
            'edit',
            'update',
            'seleccionar',
            'cargarVistaCalendario'

        ]]);
    }
    public function index()
    {
        $barrios = Barrio::all();

       return view('panelAdministrativo.barrios.index',compact('barrios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panelAdministrativo.barrios.formularios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $objBarrio = new Barrio();

        $objBarrio->name = $request['name'];
        $objBarrio->price = $request['price'];
        $objBarrio->save();

        return back()->with('flash','Barrio agregado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $objBarrio = Barrio::findOrFail($id);

       return view('panelAdministrativo.barrios.formularios.update',compact('objBarrio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objBarrio = Barrio::findOrFail($id);

        $objBarrio->name = $request['name'];
        $objBarrio->price = $request['price'];

        $objBarrio->save();

        return back()->with('flash','Barrio modificado correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       /*$objBarrio = Barrio::findOrFail($id);

       $objBarrio->delete();*/
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClienteRequest;
use App\models\Agenda;
use App\models\Cliente;
use App\models\Servicio;
use App\models\TipoServicio;
use App\models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Redirect;
use Mail;
use URL;


class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth_user', ['only' => [
            'seleccionar',


        ]]);
    }
    public function index()
    {
        $usuario = Usuario::findorfail(Auth::guard('web_usuario')->user()->id);

        $cli=Cliente::where('users_id',$usuario->id)->get();
        $id ="";

        foreach ($cli as $c){
            $id = $c->id;
        }

        $citas = Agenda::where('clients_id',$id)->get();


       return view('panelAdministrativo.cliente.dashboard',compact('citas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {

       /* $validator =  Validator::make($request->all(), [
            'email'             => 'required|unique:users,email',
            'password'          => 'required',
            'phone'             => 'required|unique:users,phone',
            'identification'    => 'required|unique:users,identification',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('flash','Los datos ingresados existen en nuestro sistema');
            return redirect('/inicio')
                ->withErrors($validator)
                ->withInput();
        }]*/


     /*   $this->validate($request, [
            'email'             => 'required|unique:users,email ',
            'password'          => 'required'
        ]);
*/
        //usuario
        $objUsuario = new Usuario();
        $objUsuario->name = $request['nombre'];
        $objUsuario->email = $request['email'];
        $objUsuario->password = $request['password'];
        $objUsuario->phone = $request['telefono'];
        $objUsuario->verified = 0;
        $objUsuario->type_user = "Cliente";
        $objUsuario->save();

        //relacion para la tabla clientes
        $objCliente = new Cliente();
        $objCliente->address_reference = $request['punto_referencia'];
        $objCliente->address = $request['direccion'];
        $objCliente->users_id = $objUsuario->id;
        $objCliente->districts_id = $request['barrio'];
        $objCliente->birthday = $request['fecha'];
        $objCliente->identification = $request['cc'];
        $objCliente->save();

        if(Auth::guard('web_usuario')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {


            if (Auth::guard('web_usuario')->user()->type_user == "Cliente"){

                return redirect()->intended('panel-cliente/seleccionar-servicio');

            }

        }
//
//
//        return redirect('panel-cliente');
        return back()->with('flash','Su cuenta se ha creado con exito!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function seleccionar(){

        $objServicios = Servicio::all();
        $objTipoServicio = TipoServicio::all();

        $usuario = Auth::guard('web_usuario')->user()->id;
        $cliente = Cliente::where('users_id',$usuario)->get();

        foreach ($cliente as $cl){
            $barrio = $cl->barrio->price;
        }

        return view('panelAdministrativo.cliente.seleccionarServicios',compact('objServicios','objTipoServicio','barrio'));
    }

    public function cargarVistaCalendario()
    {
        return view('panelAdministrativo.cliente.index');
    }

    public function perfil(){
        return view('panerlAdministrativo.cliente.perfil');
    }
}

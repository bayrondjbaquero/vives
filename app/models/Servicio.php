<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Servicio extends Model
{

    use Sluggable;

    protected $table = "services";

    protected $fillable = [
        'name',
        'num_hour',
        'slug',
        'price',
        'type_services_id'

    ];
//mostrar puntos de mil en los precrios
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function tipoServicios(){

        return $this->belongsTo('App\models\TipoServicio' , 'type_services_id');

    }

    public function agendas(){
        return $this->belongsToMany('App\models\Agenda');
    }
/*
    public function getPriceAttribute($val){
        return str_replace('.','',$val);
    }*/
}

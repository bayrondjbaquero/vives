<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = "employees";

    protected $fillable = [
        'users_id',

    ];

    public function usuario(){

        return $this->belongsTo('App\models\Usuario' , 'users_id');

    }

    public function agenda(){
        return $this->hasMany('App\models\Agenda');
    }
}

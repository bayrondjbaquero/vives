<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Administrador extends Model
{
    protected $table = "admins";

    protected $fillable = [
        'users_id',

    ];

    public function usuario(){

        return $this->belongsTo('App\models\Usuario' , 'users_id');

    }
}

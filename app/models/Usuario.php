<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Usuario extends Authenticatable
{
    protected $table = "users";

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'verified',
        'type_user',
        'identification',
        'photo',
    ];

    protected $hidden = ['password'];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value, 'UTF-8');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function administradores()
    {
        return $this->hasMany('App\models\Administrador');
    }

    public function empleados()
    {
        return $this->hasMany('App\models\Empleados');
    }

    public function clientes()
    {
        return $this->hasMany('App\models\Cliente');
    }
}

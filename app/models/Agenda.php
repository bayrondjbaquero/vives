<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = "agendas";

    protected $fillable = [
        'hour_start',
        'hour_end',
        'date',
        'price_total',
        'service_extra',
        'status',
        'employees_id',
        'servicios',
        'clients_id',
        'largo_cabello',
        'color_actual',
        'color_aplicar'

    ];

    public function usuario(){

        return $this->belongsTo('App\models\Usuario' , 'users_id');

    }

    public function trabajador(){
        return $this->belongsTo('App\models\Empleado','employees_id');
    }

    public function servicios(){
        return $this->belongsToMany('App\models\Servicio');
    }

    public function horario(){
        return $this->hasOne('App\models\Horario');
    }

    public function clientes(){
        return $this->belongsTo('App\models\Cliente','clients_id');
    }
}

<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Barrio extends Model
{

    use Sluggable;

    protected $table = "districts";

    protected $fillable = [
        'name',
        'slug',
        'price'
    ];

    public function barrio(){
        return $this->hasOne('App\models\Barrio');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
/*
    public function getPriceAttribute($val){
        return str_replace('.','',$val);
    }*/
}

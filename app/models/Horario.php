<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = "shedules";

    protected $fillable = [
        'agendas_id',
        'eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen'

    ];

    public function agenda(){

        return $this->belongsTo('App\models\Agenda' , 'agendas_id');

    }
}

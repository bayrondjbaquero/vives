<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    protected $table = "configs";

    protected $fillable = [
        'transports_price',
    ];

}

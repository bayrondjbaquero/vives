<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "clients";

    protected $fillable = [
        'users_id',
        'address',
        'address_reference',
        'birthday',
        'districts_id',
        'identification'

    ];

    public function usuario(){

        return $this->belongsTo('App\models\Usuario' , 'users_id');

    }
    public function barrio(){

        return $this->belongsTo('App\models\Barrio' , 'districts_id');

    }

    public function agenda(){
        return $this->hasMany('App\models\Agenda');
    }
}

<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class TipoServicio extends Model
{
    use Sluggable;

    protected $table = "type_services";

    protected $fillable = [
        'name',
        'slug',

    ];

    public function servicios(){
        return $this->hasMany('App\models\Servicio','type_services_id');
    }


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');
Route::get('/terminos-y-condiciones', 'MainController@terminos')->name('terminos-y-condiciones');
Route::get('/politicas-de-privacidad', 'MainController@politicas')->name('politicas-de-privacidad');
Route::resource('/contacto', 'ContactoController');
Route::post('pqr', 'ContactoController@pqr')->name('pqr');

Route::get('inicio',  'LoginController@cargarVistaLogin')->name('inicio');
Route::get('logout',	'LoginController@logout')->name('logout');
Route::resource('log',	'LoginController');
Route::post('agregarCliente', 'ClienteController@store')->name('agregarCliente');


Route::get('panel-administrativo',  'LoginController@cargarPanel')->name('panel-administrativo');
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'panel-administrativo'], function () {
    Route::resource('/administrador', 'AdministradorController');
    Route::get('/listado-clientes', 'AdministradorController@listadoClientes');
    Route::get('/listado-empleados', 'AdministradorController@listadoEmpleados');
    Route::get('/listado-empleados/create', 'AdministradorController@formEmpleado');
    Route::post('/listado-empleados/guardar','AdministradorController@guardarEmpleado');
    Route::get('/listado-empleados/{id}','AdministradorController@verEmpleado');
    Route::PUT('/editarEmpleado/{id}','AdministradorController@editarEmpleado')->name('editarEmpleado');
    Route::GET('/tarjeta/{id}','AdministradorController@tarjeta')->name('tarjeta');
    Route::resource('/barrios',	'BarrioController');
    Route::resource('/tipo-de-servicios','TipoServicioController');
    Route::resource('/servicios','ServicioController');
    Route::resource('/configuracion','ConfiguracionController');
    Route::get('/listado-citas', 'AdministradorController@listadoCitas');
    Route::get('/asignar/{id}', 'AdministradorController@asignar');
    Route::POST('/asignarTrabajador', 'AdministradorController@asignarTrabajador')->name('asignarTrabajador');
});

Route::group(['prefix' => 'panel-trabajador'], function () {
    Route::get('/citas', 'EmpleadoController@index');
});

Route::get('recuperar-contrasena',function (){
    return view('panelAdministrativo.cliente.resetPassword');
});

Route::get('cambio-contrasena',function (){
    return view('panelAdministrativo.cliente.cambioPassword');
});

Route::get('fin-agenda', function (){
    return view('panelAdministrativo.cliente.fin');
});

Route::get('panel-cliente/seleccionar-servicio','ClienteController@seleccionar');
Route::get('planel-cliente/perfil','ClienteController@perfil')->name('clientePerfil');
Route::get('panel-cliente/seleccionar-calendario','ClienteController@cargarVistaCalendario');
Route::post('/guardar/{horas}/{precio}','AgendaController@store')->name('guardar');
Route::resource('panel-cliente','ClienteController');
Route::post('solicitudCambio','LoginController@solicitudCambio')->name('solicitudCambio');
Route::post('cambioPassword','LoginController@cambiarPassword')->name('cambioPassword');
Route::get('crear-cliente',function (){
    $est = new \App\Models\Usuario();

    $est->name = "cliente";
        $est->email = "cliente@cliente.com";
        $est->password ="123";
        $est->phone ="1234";
        $est->verified =0;
        $est->type_user ="cliente";

    $est->save();



});
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: vivesbelleza
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_admins_usuarios1_idx` (`users_id`),
  CONSTRAINT `fk_admins_usuarios1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,9,'2018-01-09 09:56:56','2018-01-09 09:56:56');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agendas`
--

DROP TABLE IF EXISTS `agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hour_start` time DEFAULT NULL,
  `hour_end` time DEFAULT NULL,
  `date` date NOT NULL,
  `price_total` double NOT NULL,
  `service_extra` longtext,
  `employees_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_agendas_employees1_idx` (`employees_id`),
  CONSTRAINT `fk_agendas_employees1` FOREIGN KEY (`employees_id`) REFERENCES `employees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agendas`
--

LOCK TABLES `agendas` WRITE;
/*!40000 ALTER TABLE `agendas` DISABLE KEYS */;
INSERT INTO `agendas` VALUES (1,'01:00:00','01:00:00','2018-01-09',250,NULL,NULL,1,'2018-01-09 14:52:18',NULL),(2,'01:00:00','01:00:00','2018-01-09',250,NULL,NULL,1,'2018-01-09 15:09:25',NULL),(3,'01:00:00','01:00:00','2018-01-09',250,NULL,NULL,1,'2018-01-09 15:15:40',NULL),(4,'01:00:00','01:00:00','2018-01-09',250,NULL,NULL,1,'2018-01-09 15:32:08',NULL),(5,'01:00:00','01:00:00','2018-01-09',250,NULL,NULL,1,'2018-01-09 15:33:20',NULL),(6,'09:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 11:09:52','2018-01-10 11:09:52'),(7,'11:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 11:22:01','2018-01-10 11:22:01'),(8,'12:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 11:22:47','2018-01-10 11:22:47'),(9,'08:00:00',NULL,'2018-01-10',50,NULL,NULL,1,'2018-01-10 15:01:09','2018-01-10 15:01:09'),(10,'08:00:00',NULL,'2018-01-10',50,NULL,NULL,1,'2018-01-10 15:14:36','2018-01-10 15:14:36'),(11,'10:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 16:14:07','2018-01-10 16:14:07'),(12,'05:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 17:11:25','2018-01-10 17:11:25'),(13,'05:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 17:11:27','2018-01-10 17:11:27'),(14,'05:00:00',NULL,'2018-01-10',250,NULL,NULL,1,'2018-01-10 17:13:28','2018-01-10 17:13:28'),(15,'10:00:00',NULL,'2018-01-11',250,NULL,NULL,1,'2018-01-11 07:47:53','2018-01-11 07:47:53'),(16,'10:00:00',NULL,'2018-01-11',250,NULL,NULL,1,'2018-01-11 07:47:59','2018-01-11 07:47:59'),(17,'10:00:00',NULL,'2018-01-11',250,NULL,NULL,1,'2018-01-11 07:48:00','2018-01-11 07:48:00');
/*!40000 ALTER TABLE `agendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `address_reference` text,
  `birthday` date NOT NULL,
  `identification` varchar(45) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `districts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_clientes_usuarios_idx` (`users_id`),
  KEY `fk_clients_districts1_idx` (`districts_id`),
  CONSTRAINT `fk_clientes_usuarios` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clients_districts1` FOREIGN KEY (`districts_id`) REFERENCES `districts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (6,14,'sjsa','dsjf','2018-01-09','32','2018-01-09 10:21:39','2018-01-09 10:21:39',1);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configs` (
  `id` int(11) NOT NULL,
  `transports_price` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs`
--

LOCK TABLES `configs` WRITE;
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '	',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `districts`
--

LOCK TABLES `districts` WRITE;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
INSERT INTO `districts` VALUES (1,'Bosque','bosque','2018-01-03 09:16:43','2018-01-03 09:16:43'),(2,'manga','manga','2018-01-09 09:17:36','2018-01-09 09:17:36');
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_empleados_usuarios1_idx` (`users_id`),
  CONSTRAINT `fk_empleados_usuarios1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,1,'2018-01-03 09:10:57','2018-01-03 09:10:57'),(2,15,'2018-01-10 15:01:44','2018-01-10 15:01:44'),(3,16,'2018-01-10 17:13:20','2018-01-10 17:13:20');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eight` enum('1','0') DEFAULT NULL,
  `nine` enum('1','0') DEFAULT NULL,
  `ten` enum('1','0') DEFAULT NULL,
  `eleven` enum('1','0') DEFAULT NULL,
  `twelve` enum('1','0') DEFAULT NULL,
  `thirteen` enum('1','0') DEFAULT NULL,
  `fourteen` enum('1','0') DEFAULT NULL,
  `fifteen` enum('1','0') DEFAULT NULL,
  `sixteen` enum('1','0') DEFAULT NULL,
  `seventeen` enum('1','0') DEFAULT NULL,
  `eighteen` enum('1','0') DEFAULT NULL,
  `agendas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_schedules_agendas1_idx` (`agendas_id`),
  CONSTRAINT `fk_schedules_agendas1` FOREIGN KEY (`agendas_id`) REFERENCES `agendas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `num_hour` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `type_services_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_services_type_services1_idx` (`type_services_id`),
  CONSTRAINT `fk_services_type_services1` FOREIGN KEY (`type_services_id`) REFERENCES `type_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'keratina',6,'keratina',1,250,'2018-01-03 16:01:31','2018-01-10 15:35:35'),(2,'Manicure',1,'unas-manos',2,17,'2018-01-04 14:49:55','2018-01-10 15:39:58'),(3,'Pedicure',1,'unas-pie',2,20,'2018-01-04 14:50:16','2018-01-10 15:40:26'),(4,'Peinado damas',2,'peinado-damas',1,50,'2018-01-04 14:50:49','2018-01-04 14:50:49'),(5,'Masocapiloterapia',2,'masocapiloterapia',1,110,'2018-01-10 15:25:19','2018-01-10 15:25:19'),(6,'Blower Semi permanente',5,'blower-semipermanente',1,90,'2018-01-10 15:26:18','2018-01-10 15:35:52'),(7,'Depilación cera dos mini zonas (Bozo, cejas, axila, patilla)',1,'depilacion-cera-dos-mini-zonas-bozo-cejas-axila-patilla',3,25,'2018-01-10 15:29:36','2018-01-10 15:29:36'),(8,'Depilacion cera Bikini',1,'depilacion-cera-bikini',3,25,'2018-01-10 15:30:09','2018-01-10 15:30:09'),(9,'Cejas semipermanentes',1,'cejas-semipermanentes',4,25,'2018-01-10 15:31:42','2018-01-10 15:31:42'),(10,'Pestañas punto a punto',1,'pestanas-punto-a-punto',4,25,'2018-01-10 15:33:21','2018-01-10 15:33:21'),(11,'Maquillaje para toda ocasion con hidratación',3,'maquillaje-para-toda-ocasion-con-hidratacion',4,120,'2018-01-10 15:34:49','2018-01-10 15:34:49'),(12,'Uñas postizas',1,'unas-postizas',2,40,'2018-01-10 15:44:01','2018-01-10 15:46:52'),(13,'Uñas acrilicas',3,'unas-acrilicas',2,110,'2018-01-10 15:44:34','2018-01-10 15:44:34'),(14,'Uñas en Gel',3,'unas-en-gel',2,120,'2018-01-10 15:45:38','2018-01-10 15:45:38'),(15,'Uñas esmalte semipermanente',2,'unas-esmalte-semipermanente',2,60,'2018-01-10 15:46:09','2018-01-10 15:46:09');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_has_agendas`
--

DROP TABLE IF EXISTS `services_has_agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_has_agendas` (
  `services_id` int(11) NOT NULL,
  `agendas_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`services_id`,`agendas_id`),
  KEY `fk_services_has_agendas_agendas1_idx` (`agendas_id`),
  KEY `fk_services_has_agendas_services1_idx` (`services_id`),
  CONSTRAINT `fk_services_has_agendas_agendas1` FOREIGN KEY (`agendas_id`) REFERENCES `agendas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_services_has_agendas_services1` FOREIGN KEY (`services_id`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_has_agendas`
--

LOCK TABLES `services_has_agendas` WRITE;
/*!40000 ALTER TABLE `services_has_agendas` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_has_agendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_services`
--

DROP TABLE IF EXISTS `type_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_services`
--

LOCK TABLES `type_services` WRITE;
/*!40000 ALTER TABLE `type_services` DISABLE KEYS */;
INSERT INTO `type_services` VALUES (1,'Cuidado Capilar','cuidado-capilar','2018-01-03 15:44:51','2018-01-03 15:44:51'),(2,'Cuidado de uñas','cuidado-de-unas','2018-01-03 15:45:10','2018-01-03 15:45:10'),(3,'Cuidado Corporal','cuidado-corporal','2018-01-03 15:45:22','2018-01-03 15:45:22'),(4,'Cuidado Facial','cuidado-facial','2018-01-03 15:45:33','2018-01-03 15:45:33');
/*!40000 ALTER TABLE `type_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `email` varchar(85) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(75) DEFAULT NULL,
  `verified` tinyint(4) NOT NULL COMMENT '0 - Sin verificar\n1 - Verificado',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type_user` enum('Empleado','Administrador','Cliente') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'empleado','empleado@empleado.com','$2y$10$5skM2CjCR7wxmMZCAf1hv.vJkkEUe5e0iUmeJZmSHbHiBWYOFZ5w6','12345',1,'3gd0qT1iKtu5JOf2LghEy9CuJonFwgchXQ8ToYsQbtcGuBDgvhEXMNcWw9ru','2018-01-03 09:10:57','2018-01-09 10:59:13',NULL,'Administrador'),(9,'admin','admin@admin.com','$2y$10$23/EOKyohMkSfGdi6NAda.6kZMO.HUPjfiRR2cPUxpG61kZqw5r8C','1432',1,NULL,'2018-01-09 09:56:56','2018-01-09 09:56:56',NULL,'Administrador'),(14,'empleado','em@em.com','$2y$10$uZhpHZ9KO81ZGvfIKYt4jOJppxPdC1H8MrpPPEEG1yIy0Zaul1xj.','34875',0,NULL,'2018-01-09 10:21:39','2018-01-09 10:21:39',NULL,'Cliente'),(15,'empleado2','em2@em.com','$2y$10$tQ469bEL2mDB6sRgPQN18.0LfgnJccwF7HWeWW3b1Tsnk1BdxUmsO','34532',0,NULL,'2018-01-10 15:01:44','2018-01-10 15:01:44',NULL,'Empleado'),(16,'empleado 3','carlos@carlos.com','$2y$10$5.U5He1ntDOmcqd9.lRHRu0jgtSSYQRXm6jeKN.91Dcni3BKL4CJS','324',0,NULL,'2018-01-10 17:13:20','2018-01-10 17:13:20',NULL,'Empleado');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-08 10:27:10
